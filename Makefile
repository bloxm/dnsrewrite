
##############################################
# OpenWrt Makefile for dnsrewrite program
#
#
# Most of the variables used here are defined in
# the include directives below. We just need to 
# specify a basic description of the package, 
# where to build our program, where to find 
# the source files, and where to install the 
# compiled program on the router. 
# 
# Be very careful of spacing in this file.
# Indents should be tabs, not spaces, and 
# there should be no trailing whitespace in
# lines that are not commented.
# 
##############################################

include $(TOPDIR)/rules.mk

# Name and release number of this package
PKG_NAME:=dnsrewrite
PKG_RELEASE:=1


# This specifies the directory where we're going to build the program.  
# The root build directory, $(BUILD_DIR), is by default the build_mipsel 
# directory in your OpenWrt SDK directory
PKG_BUILD_DIR := $(BUILD_DIR)/$(PKG_NAME)


include $(INCLUDE_DIR)/netfilter.mk
include $(INCLUDE_DIR)/uclibc++.mk
include $(INCLUDE_DIR)/package.mk



# Specify package information for this program. 
# The variables defined here should be self explanatory.
# If you are running Kamikaze, delete the DESCRIPTION 
# variable below and uncomment the Kamikaze define
# directive for the description below
define Package/dnsrewrite
	SECTION:=utils
	CATEGORY:=Utilities
	TITLE:=dnsrewrite -- sorts it
	DEPENDS:=+uclibcxx \
	    +libnetfilter-queue \
	    +libpthread \
	    +libjson-c
endef

#To add a dependency, modify DEPENDS, above
#This adds a dependency to our custom library - +libspcdns+codec



# Uncomment portion below for Kamikaze and delete DESCRIPTION variable above
define Package/dnsrewrite/description
endef


# Specify what needs to be done to prepare for building the package.
# In our case, we need to copy the source files to the build directory.
# This is NOT the default.  The default uses the PKG_SOURCE_URL and the
# PKG_SOURCE which is not defined here to download the source from the web.
# In order to just build a simple program that we have just written, it is
# much easier to do it this way.
define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	mkdir -p $(PKG_BUILD_DIR)/iptables
	$(CP) ./src/* $(PKG_BUILD_DIR)/
	$(CP) ./src/iptables/* $(PKG_BUILD_DIR)/iptables/
endef


# We do not need to define Build/Configure or Build/Compile directives
# The defaults are appropriate for compiling a simple program such as this one


# Specify where and how to install the program. Since we only have one file, 
# the dnsrewrite executable, install it by copying it to the /bin directory on
# the router. The $(1) variable represents the root directory on the router running 
# OpenWrt. The $(INSTALL_DIR) variable contains a command to prepare the install 
# directory if it does not already exist.  Likewise $(INSTALL_BIN) contains the 
# command to copy the binary file from its current location (in our case the build
# directory) to the install directory.
define Package/dnsrewrite/install
	$(INSTALL_DIR) $(1)/bin
	$(INSTALL_DIR) $(1)/bin/bloxm
	$(INSTALL_DIR) $(1)/bin/bloxm/syscfg
	$(INSTALL_DIR) $(1)/bin/bloxm/iptables
	$(INSTALL_DIR) $(1)/etc/dnsrewrite/dnsproviders
	$(INSTALL_DIR) $(1)/etc/dnsrewrite/catproviders
	$(INSTALL_DIR) $(1)/etc/dnsrewrite/default-policy
	$(INSTALL_DIR) $(1)/etc/dnsrewrite/users
	$(INSTALL_DIR) $(1)/etc/dnsrewrite/policies
	$(INSTALL_BIN) $(TOPDIR)/.config $(1)/
	gzip -9 $(1)/.config
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/tcpproxy $(1)/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/dnsrewrite $(1)/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/dnsmirror $(1)/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/fsjson/fsjson $(1)/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/iptables/* $(1)/bin/bloxm/iptables/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/scripts/*.sh $(PKG_BUILD_DIR)/scripts/iptables-configure $(1)/bin/bloxm/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/scripts/*.inc $(PKG_BUILD_DIR)/scripts/iptables-configure $(1)/bin/bloxm/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/syscfg/* $(1)/bin/bloxm/syscfg/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/policies/default/* $(1)/etc/dnsrewrite/default-policy/
	cp -R $(PKG_BUILD_DIR)/dnsproviders/* $(1)/etc/dnsrewrite/dnsproviders/
	cp -R $(PKG_BUILD_DIR)/catproviders/* $(1)/etc/dnsrewrite/catproviders/


endef

define Package/dnsrewrite/postinst
#!/bin/bash

exec > /tmp/postinst
exec 2>&1
set -x
env

if [[ "L${OPENWRT_BUILD}" == "L1" ]] ; then
    $${IPKG_INSTROOT}/bin/bloxm/syscfg/syscfg.sh
else
    /bin/bloxm/syscfg/syscfg.sh
fi

endef

define Package/dnsrewrite/clean
endef

# This line executes the necessary commands to compile our program.
# The above define directives specify all the information needed, but this
# line calls BuildPackage which in turn actually uses this information to
# build a package.
$(eval $(call BuildPackage,dnsrewrite))
