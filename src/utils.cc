/* returns packet id */

#include "utils.h"
#include <string.h>
#include <stdio.h>
#include<stdarg.h>


FILE *logfp=0;
int cfgLogLevel=0;

void openLogFile(const char *path, const char *mode)
{
    if(strcmp(path, "stderr") == 0)
        logfp=stderr;
    else if(strcmp(path, "stdout") == 0)
        logfp=stdout;
    else
        logfp=fopen(path, mode);
}

void closeLogFile()
{
    fclose(logfp);
}

void setLogLevel(int level)
{
    cfgLogLevel = level;
}

extern "C"
{
int printFILE(FILE* fp, const char *format, va_list arglist)
{
    if(!fp)
        return -1;

    int ret = vfprintf(fp, format, arglist);

    fflush(logfp);
    return ret;
}

int _logprintf(int logLevel, const char *format, ...)
{
    if(logLevel > cfgLogLevel)
        return 0;

    va_list arglist;
    va_start(arglist, format);

    int ret = printFILE(logfp, format, arglist);
    fflush(logfp);

    va_end(arglist);

    return ret;
}

int printfile(const char *file, const char *mode, const char *format, ...)
{
    va_list arglist;
    va_start(arglist, format);

    FILE *fp;
	if((fp = fopen(file, mode)) == NULL) 
    {
		return(false);
	}

    int ret = printFILE(fp, format, arglist);
    va_end(arglist);

    fclose(fp);

    return ret;
}

} //extern "C"

bool get_state(const char *file, char * buf, uint8_t bufsize)
{
    FILE *fp;
	if((fp = fopen(file, "r")) == NULL) 
    {
		return(false);
	}

    char *p = fgets(buf, bufsize, fp);

    fclose(fp);

    return(p == buf);
}

bool find_in_file(const char *file, const char *str) 
{
	FILE *fp;
	char temp[128];
	
    logprintf("find_in_file(%s, %s)\n", file, str);
	if((fp = fopen(file, "r")) == NULL) 
    {
		return(false);
	}

	while(fgets(temp, sizeof(temp), fp) != NULL) 
    {
		if((strcasestr(temp, str)) != NULL) 
        {
            fclose(fp);
            logprintf("%s found in file %s\n", str, file);
            return true;
		}
	}

    logprintf("%s NOT found in file %s\n", str, file);
   	return(false);
}


uint32_t checksum(const void *buf, size_t len, uint32_t sum) 
{
    const char *p = reinterpret_cast<const char*>(buf);
    size_t i;

    for (i = 0; i < (len & ~1); i += 2) {
        sum += ntohs(*(uint16_t *)(p + i));
        if (sum > 0xffff)
            sum -= 0xffff;
    }

    if (i < len) {
        uint16_t val = p[i];
        val <<= 8;
        sum += val;
        if (sum > 0xffff)
            sum -= 0xffff;
    }

    return sum;
}

uint32_t wrapsum(uint32_t sum) 
{
	sum = ~sum & 0xffff;
	return htons(sum);
}

void calcChecksum(const void *payload, size_t len, struct ip * ip_, struct udphdr * udp_)
{
	ip_->ip_sum = 0;
	udp_->check = 0;

	ip_->ip_sum = wrapsum(checksum(ip_, sizeof(*ip_), 0));
	udp_->check = wrapsum(
		      checksum(
			udp_, sizeof(*udp_),
			checksum(
			    payload, len,
				checksum(
				    &ip_->ip_src,
				    2 * sizeof(ip_->ip_src),
				    IPPROTO_UDP + (uint32_t)ntohs(udp_->len)
		)
	    )
	));
}


int dns_strlen(unsigned char *dnsString, bool *pbCompressed)
{
    //Handles compressed strings
    //
    int len = 0;
    uint8_t octet = *dnsString;
    if((octet & 0xC0) == 0xC0)
    {
//        logprintf("Reference %d\n", ntohs(*((uint16_t*)dnsString)));
        if(pbCompressed)
            *pbCompressed = true;
        return 1;
    }

//    logprintf("first octet %d %2.2x\n", octet, octet);
    while(octet != 0)
    {
        len+=octet + 1;
        dnsString+=octet + 1;
        octet = *dnsString;

        if((octet & 0xC0) == 0xC0)
        {
            ++len;
            if(pbCompressed)
                *pbCompressed = true;
            break;
        }
//        logprintf("next octet %d - length currently %d\n", octet, len);
    }

    return len;
}


void dns_strcpy(DNSHeader *pDNSHeader, char * dest, unsigned char *dnsString, int bufsize)
{
    uint8_t octet = *(dnsString++);
    char *pDest = dest;
    
    while(octet != 0 && bufsize > 2)
    {
        if((octet & 0xC0) == 0xC0)
        {
            //get the offset, carry on from there
            uint16_t offset = ntohs(*(uint16_t*)(dnsString-1));
            offset &= ~0xC000;

            return dns_strcpy(pDNSHeader, pDest, (unsigned char*)pDNSHeader + offset, bufsize);
        }
        else
        {
            for(uint8_t i = 0; i < octet && bufsize > 1; ++i, --bufsize)
            {
                *(pDest++) = (char)*(dnsString++);
            }
            if(bufsize > 2)
            {
                octet = *(dnsString++);
                if(octet)
                {
                    *(pDest++) = '.';
                }
            }
        }
    }
    *(pDest++) = '\0';
}


//Copy from DNS format, to string
char *dns_strcpy(char *dest, const unsigned char *dnsString)
{
    uint8_t octet = *dnsString;
    if((octet & 0xC0) == 0xC0)
    {
        return NULL;
    }

    dnsString++;
    char *pDest = dest;

    while(octet != 0)
    {
        for(uint8_t i = 0; i < octet; ++i)
        {
            *(pDest++) = (char)*(dnsString++);
        }
        octet = *(dnsString++);
        if(octet)
        {
            *(pDest++) = '.';
        }
    }
    *(pDest++) = '\0';

    return dest;
}

//Copy from string to DNS format
unsigned char *dns_strcpy(unsigned char *dnsString, const char *src)
{
    uint8_t octet = 0;
    const char *pWord = src;
    const char *pCur = pWord;
    unsigned char *pDest = dnsString;

    while(*pCur != '\0')
    {
        pWord = pCur;
        while(*pCur != '.' && *pCur != '\0')
        {
            pCur++;
        }
        octet = (pCur - pWord);
        *(pDest++) = octet;
//        strncpy((char*)pDest, pWord, octet);
        memcpy((void*)pDest, (void*)pWord, octet);
        pDest += octet;

        if(*pCur == '\0')
        {
            *pDest = '\0';
            return dnsString;
        }
        else
        {
            pCur++;
            pWord++;
        }
    }
    return dnsString;
}


bool setupDNSPacketPointers(unsigned char *pIPPacket,
                            unsigned char **ppUDPPacket,
                            unsigned char **ppDNSPacket,
                            DNSHeader **dnsHeader,
                            unsigned char **ppDNSQuestion,
                            DNSQuestion **dnsQuestion,
                            unsigned char **ppDNSAnswer,
                            DNSAAnswer **dnsAAnswer,
                            unsigned char **ppDNSFirstAnswer)
{
    *ppUDPPacket = pIPPacket + (((iphdr*)pIPPacket)->ihl * 4);
    *ppDNSPacket = *ppUDPPacket + sizeof(udphdr);
    *dnsHeader = (DNSHeader*)*ppDNSPacket;

    if((*dnsHeader)->qr == 1)
    {
        //Only handle one question and one answer for now
        if(ntohs((*dnsHeader)->qdcount) == 1 && ntohs((*dnsHeader)->ancount) == 1 )
        {
            *ppDNSQuestion = *ppDNSPacket + sizeof(DNSHeader);
            *dnsQuestion = (DNSQuestion*)(*ppDNSQuestion + dns_strlen(*ppDNSQuestion) + 1);
            *ppDNSAnswer = ((unsigned char*)*dnsQuestion) + sizeof(DNSQuestion);
            *ppDNSFirstAnswer = *ppDNSAnswer;
            //The name in the answer section may be a reference/compressed - ignore it
            *dnsAAnswer = (DNSAAnswer*)(*ppDNSAnswer + dns_strlen(*ppDNSAnswer) + 1);
            return true;
        }
        else if(ntohs((*dnsHeader)->qdcount) == 1)
        {
//            logprintf("Trying to handle %d questions and %d answers\n", ntohs((*dnsHeader)->qdcount), ntohs((*dnsHeader)->ancount));
            *ppDNSQuestion = *ppDNSPacket + sizeof(DNSHeader);
            *dnsQuestion = (DNSQuestion*)(*ppDNSQuestion + dns_strlen(*ppDNSQuestion) + 1);

            //Lets find the first A record answer
            bool isARecord = false;
            uint16_t cnt = ntohs((*dnsHeader)->ancount);

            *ppDNSAnswer = ((unsigned char*)*dnsQuestion) + sizeof(DNSQuestion);
            *ppDNSFirstAnswer = *ppDNSAnswer;
            //The name in the answer section may be a reference/compressed - ignore it
            *dnsAAnswer = (DNSAAnswer*)(*ppDNSAnswer + dns_strlen(*ppDNSAnswer) + 1);

            while(!isARecord && cnt)
            {
                --cnt;
                if(ntohs((*dnsAAnswer)->type) == 1)
                {
                    logprintf("Found an A record\n");
                    isARecord = true;
                }
                else
                {
                    logprintf("Skipping record of type %d\n", ntohs((*dnsAAnswer)->type));
                    *ppDNSAnswer = ((unsigned char*)*dnsAAnswer) + (sizeof(DNSAnswer) + ntohs((*dnsAAnswer)->length));
                    //The name in the answer section may be a reference/compressed - ignore it
                    *dnsAAnswer = (DNSAAnswer*)((*ppDNSAnswer) + dns_strlen(*ppDNSAnswer) + 1);
                }
            }
            if(isARecord)
            {
                return true;
            }
            else
            {
                logprintf("Failed to find A record\n");
            }
        }
        else
        {
            logprintf("ERROR cant handle %d questions and %d answers\n", ntohs((*dnsHeader)->qdcount), ntohs((*dnsHeader)->ancount));
            //*ppDNSQuestion = *ppDNSPacket + sizeof(DNSHeader);
            //*dnsQuestion = (DNSQuestion*)(*ppDNSQuestion + dns_strlen(*ppDNSQuestion) + 1);
            //*ppDNSAnswer = ((unsigned char*)*dnsQuestion) + sizeof(DNSQuestion);
            ////The name in the answer section may be a reference/compressed - ignore it
            //*dnsAAnswer = (DNSAAnswer*)(*ppDNSAnswer + dns_strlen(*ppDNSAnswer) + 1);
            //return true;
        }
    }
    *ppDNSQuestion = NULL;
    *dnsQuestion = NULL;
    *ppDNSAnswer = NULL;
    *dnsAAnswer = NULL;

    return false;
}


char szDump[32768];
char * dumpBytes(const unsigned char *packet_, int length_)
{
    memset(szDump, 0, sizeof(szDump));

    if(!packet_ || !length_)
        return szDump;

    char *sIndex = szDump;
    int nCount=0;


    while(nCount < length_)
    {
        for(int j=0; j < 2; j++)
        {
            if(j == 0 && nCount)
            {
                sprintf(sIndex, "\n");
                sIndex++;
            }

            for(int i=0; i < 8; i++)
            {
                if(nCount < length_)
                {
                    if(i != 0)
                    {
                        sprintf(sIndex++, " ");
                    }
                    sprintf(sIndex, "%2.2x", packet_[nCount++]);
                    sIndex +=2 ;
                }
            }
            sprintf(sIndex, "  ");
            sIndex +=2 ;
        }
    }
    return szDump;
}

