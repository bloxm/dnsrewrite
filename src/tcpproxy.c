/*
 * Modified code, originally taken from
 * ftp://ftp.measurement-factory.com/pub/simple-tcp-proxy/simple-tcp-proxy.c
 * $Id: simple-tcp-proxy.c,v 1.6 2002/11/27 00:40:31 wessels Exp $
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>
#include <signal.h>
#include <assert.h>
#include <syslog.h>
#include <stdbool.h>


#include <sys/types.h>
#include <sys/select.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/wait.h>

#include <netinet/in.h>

#include <arpa/ftp.h>
#include <arpa/inet.h>
#include <arpa/telnet.h>

#define LINE_BUF_SIZE 512
#define URL_BUF_SIZE 512
#define BUF_SIZE 1024

extern int sys_nerr, errno;

char client_hostname[64];

int nMaxConnections = 3;
int nCurrentConnections = 0;

//#define LOGGING

#ifdef LOGGING

#include<stdarg.h>

char szDump[32768];
char * dumpBytes(const unsigned char *packet_, int length_)
{
    memset(szDump, 0, sizeof(szDump));

    if(!packet_ || !length_)
        return szDump;

    char *sIndex = szDump;
    int nCount=0;


    while(nCount < length_)
    {
        int j=0;
        for(j=0; j < 2; j++)
        {
            if(j == 0 && nCount)
            {
                sprintf(sIndex, "\n");
                sIndex++;
            }

            int i=0;
            for(i=0; i < 8; i++)
            {
                if(nCount < length_)
                {
                    if(i != 0)
                    {
                        sprintf(sIndex++, " ");
                    }
                    sprintf(sIndex, "%2.2x", packet_[nCount++]);
                    sIndex +=2 ;
                }
            }
            sprintf(sIndex, "  ");
            sIndex +=2 ;
        }
    }
    return szDump;
}


FILE *logfp=0;

void openLogFile(const char *path)
{
    logfp=fopen(path,"w");
}

void closeLogFile()
{
    fclose(logfp);
}

int printFILE(FILE* fp, const char *format, va_list arglist)
{
    int ret = vfprintf(fp, format, arglist);

    fflush(logfp);
    return ret;
}

int logprintf(const char *format, ...)
{
    va_list arglist;
    va_start(arglist, format);

    int ret = printFILE(logfp, format, arglist);
    fflush(logfp);

    va_end(arglist);

    return ret;
}

#else
#define logprintf(...) 
#define openLogFile(...) 
#endif

void
cleanup(int sig)
{
    logprintf("Cleaning up...");
    exit(0);
}

void
sigreap(int sig)
{
    int status;
    pid_t p;
    while ((p = waitpid(-1, &status, WNOHANG)) > 0)
    {
        logprintf("sigreap: pid=%d, status=%d\n", (int) p, status);
        --nCurrentConnections;
    }
    /* doh! */
    signal(SIGCHLD, sigreap);
}

void
set_nonblock(int fd)
{
    int flags = fcntl(fd, F_GETFL, 0);
    flags |= O_NONBLOCK;

    if(fcntl(fd, F_SETFL, flags) != 0)
    {
        logprintf("fcntl F_SETFL: FD %d: %s", fd, strerror(errno));
        exit(1);
    }
}


int
create_server_sock(const char *addr, int port)
{
    int addrlen, s, on = 1, x;
    static struct sockaddr_in client_addr;

    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0)
        perror("socket"), exit(1);

    addrlen = sizeof(client_addr);
    memset(&client_addr, '\0', addrlen);
    client_addr.sin_family = AF_INET;
    client_addr.sin_addr.s_addr = inet_addr(addr);
    client_addr.sin_port = htons(port);
    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, 4);
    x = bind(s, (struct sockaddr *) &client_addr, addrlen);
    if (x < 0)
        perror("bind"), exit(1);

    x = listen(s, 5);
    if (x < 0)
        perror("listen"), exit(1);

    return s;
}

int
open_remote_host(const char *host, int port)
{
    struct sockaddr_in rem_addr;
    int len, s, x;
    struct hostent *H;
    int on = 1;

    H = gethostbyname(host);
    if (!H)
        return (-2);

    len = sizeof(rem_addr);

    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0)
        return s;

    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, 4);

    len = sizeof(rem_addr);
    memset(&rem_addr, '\0', len);
    rem_addr.sin_family = AF_INET;
    memcpy(&rem_addr.sin_addr, H->h_addr, H->h_length);
    rem_addr.sin_port = htons(port);
    x = connect(s, (struct sockaddr *) &rem_addr, len);
    if (x < 0)
    {
        close(s);
        return x;
    }
    set_nonblock(s);
    return s;
}

int
get_hinfo_from_sockaddr(struct sockaddr_in addr, int len, char *fqdn)
{
    struct hostent *hostinfo;

    hostinfo = gethostbyaddr((char *) &addr.sin_addr.s_addr, len, AF_INET);
    if (!hostinfo)
    {
        sprintf(fqdn, "%s", inet_ntoa(addr.sin_addr));
        return 0;
    }
    if (hostinfo && fqdn)
        sprintf(fqdn, "%s [%s]", hostinfo->h_name, inet_ntoa(addr.sin_addr));
    return 0;
}


int
wait_for_connection(int s)
{
    static int newsock;
    static socklen_t len;
    static struct sockaddr_in peer;

    len = sizeof(struct sockaddr);
    newsock = accept(s, (struct sockaddr *) &peer, &len);
    /* dump_sockaddr (peer, len); */
    if (newsock < 0)
    {
        if (errno != EINTR)
            perror("accept");
    }
    get_hinfo_from_sockaddr(peer, len, client_hostname);
    set_nonblock(newsock);
    return (newsock);
}

int
mywrite(int fd, char *buf, int *len)
{
    int x = write(fd, buf, *len);
    if (x < 0)
        return x;
    if (x == 0)
        return x;
    if (x != *len)
        memmove(buf, buf+x, (*len)-x);
    *len -= x;
    return x;
}

void
service_client(int cfd,
               const char *localurlprefix,
               const char * localaddr,
               int localport,
               const char * remoteaddr,
               int remoteport)
{
    int maxfd;
    char *sbuf;
    char *cbuf;
    int x, n;
    int cbo = 0;
    int sbo = 0;
    int sfd = 0; //The FD for the onward connection
    fd_set R;

    sbuf = (char*)malloc(BUF_SIZE);
    cbuf = (char*)malloc(BUF_SIZE);
    maxfd = cfd;
    maxfd++;

    char *szLineBuf = (char*)malloc(LINE_BUF_SIZE);
    char nLineLen=0;
//    char *szURLBuf = (char*)malloc(URL_BUF_SIZE);
    char *pIndex = szLineBuf;
    bool bURLMode = true;
    bool bHeaderMode = true;



    while (1)
    {
        struct timeval to;
        to.tv_sec = 0;
        to.tv_usec = 10000;

        FD_ZERO(&R);
        if (cbo < BUF_SIZE)
            FD_SET(cfd, &R);

        if(!bHeaderMode)
        {
            if (sbo < BUF_SIZE)
                FD_SET(sfd, &R);

            if (cbo)
            {
                if (mywrite(sfd, cbuf, &cbo) < 0 && errno != EWOULDBLOCK)
                {
                    logprintf("write %d: %s\n", sfd, strerror(errno));
                    exit(1);
                }
            }
            if (sbo)
            {
                if (mywrite(cfd, sbuf, &sbo) < 0 && errno != EWOULDBLOCK)
                {
                    logprintf("write %d: %s\n", cfd, strerror(errno));
                    exit(1);
                }
            }
        }

        x = select(maxfd+1, &R, 0, 0, &to);
        if (x > 0)
        {
            if (FD_ISSET(cfd, &R))
            {
                n = read(cfd, cbuf+cbo, BUF_SIZE-cbo);
                logprintf("read %d: %d bytes\n", cfd, n);
                int cnt=0;
                char *pCur = cbuf+cbo;
                while(bHeaderMode && cnt <= n)
                {
                    if(bHeaderMode && n > 0)
                    {
                        while(cnt <= n)
                        {
                            ++cnt;
                            if(*pCur != '\n' && nLineLen < LINE_BUF_SIZE)
                            {
                                nLineLen++;
                                *pIndex++ = *pCur++;
                            }
                            else
                            {
                                *pIndex = 0;
                                break;
                            }
                        }

                        if(*pIndex == 0)
                        {
                            logprintf("Got line is %s\n", szLineBuf);


                            //Skip past the HTTP verb to get the URL
                            pCur = szLineBuf;

                            if(bURLMode)
                            {
                                while(*pCur != ' ' && *pCur != 0)
                                {
                                    ++pCur;
                                }

                                if(*pCur != ' ')
                                {
                                    logprintf("DOES NOT LOOK LIKE HTTP - %s\n", szLineBuf);
                                    _exit(0);
                                }

                                ++pCur;

                                //Make onward connection to sfd
                                logprintf("strncmp(%s, %s, %d)\n", pCur, localurlprefix, strlen(localurlprefix));
                                if(strncmp(pCur, localurlprefix, strlen(localurlprefix)) == 0)
                                {
                                    logprintf("Connection to %s:%d\n", localaddr, localport);
                                    sfd = open_remote_host(localaddr, localport);
                                }
                                else
                                {
                                    logprintf("Connection to REMOTE %s:%d\n", remoteaddr, remoteport);
                                    sfd = open_remote_host(remoteaddr, remoteport);
                                }

                                if(sfd < 0)
                                {
                                    logprintf("Failed to make onward\n", strerror(errno));
                                    _exit(0);
                                }

                                //Update maxfd
                                maxfd = cfd > sfd ? cfd : sfd;
                                //maxfd++;
                                bURLMode = false;

                                pCur = cbuf+cbo+cnt;
                                pIndex = szLineBuf;
                                nLineLen=0;
                            }
                            else
                            {
                                //Its another header
                                logprintf("HEADER %s\n", szLineBuf);
                                const char *cHeader=   "Connection: keep-alive";
                                const char *cNewHeader="Connection: close";
                                int nStringDiff = strlen(cHeader) - strlen(cNewHeader);
                                if(strncasecmp(szLineBuf, cHeader, strlen(cHeader)) == 0)
                                {
                                    bHeaderMode = false;

                                    //cnt indexes the character after /n
                                    //int nOffset = cnt + strlen(szLineBuf) - 1;

                                    char *pOriginalHeader = cbuf + cbo + cnt;
                                    pOriginalHeader -= (strlen(szLineBuf) + 1);

                                    int headerIndex = pOriginalHeader - cbuf;

                                    int nTotalBufDataSize=cbo + n - nStringDiff;
                                    logprintf("BUF ORIGINAL HEADER COULD BE %s\n", dumpBytes(pOriginalHeader, nTotalBufDataSize));
                                    memmove (pOriginalHeader + strlen(cNewHeader), pOriginalHeader + strlen(cHeader), nTotalBufDataSize);
                                    logprintf("NEW HEADER %s\n", dumpBytes(cNewHeader, nTotalBufDataSize));
                                    memcpy(pOriginalHeader, cNewHeader, strlen(cNewHeader));
                                    //memmove (pOriginalHeader + strlen (cNewHeader) + 1, pOriginalHeader + strlen(cHeader) +1, BUF_SIZE - ( cbo + cnt) );

                                    logprintf("BUF2 ORIGINAL HEADER COULD BE %s\n", dumpBytes(pOriginalHeader, nTotalBufDataSize));
                                    //logprintf("ORIGINAL HEADER COULD BE %s\n", pOriginalHeader);
                                    n -= nStringDiff;
                                    cnt -= nStringDiff;
                                    pCur = cbuf+cbo+cnt;
                                    pIndex = szLineBuf;
                                    nLineLen=0;
                                }
                                else
                                {
                                    pCur = cbuf+cbo+cnt;
                                    pIndex = szLineBuf;
                                    nLineLen=0;
                                }
                            }
                        }
                    }
                }

                //logprintf("read %d bytes from CLIENT (%d)", n, cfd);
                if (n > 0)
                {
                    cbo += n;
                }
                else
                {
                    logprintf("Closing connection because client went away\n");
                    close(cfd);
                    if(!bURLMode)
                    {
                        close(sfd);
                    }
                    _exit(0);
                }
            }
            if(!bHeaderMode)
            {
                if (FD_ISSET(sfd, &R))
                {
                    n = read(sfd, sbuf+sbo, BUF_SIZE-sbo);
                    //logprintf("read %d bytes from SERVER (%d)\n", n, sfd);
                    if (n > 0)
                    {
                        sbo += n;
                    }
                    else if(n == 0)
                    {
                    logprintf("Closing connection because server went away %d in buffer\n", sbo);
                        close(sfd);
                        close(cfd);
                        _exit(0);
                    }
                }
            }
        }
        else if (x < 0 && errno != EINTR)
        {
            logprintf("Closing connection becuase of select %d, %d in buffer\n", sbo, cbo);
            close(sfd);
            close(cfd);
            _exit(0);
        }
    }
}


int
main(int argc, char *argv[])
{
    if (8 != argc)
    {
        fprintf(stderr, "usage: %s bindaddr, bindport, localurlprefix, localaddr localport remotehost remoteport\n", argv[0]);
        exit(1);
    }

    const char *bindaddr = strdup(argv[1]);
    int bindport = atoi(argv[2]);

    const char *localurlprefix =  strdup(argv[3]);

    const char *localaddr = strdup(argv[4]);
    int localport = atoi(argv[5]);

    const char *remoteaddr = strdup(argv[6]);
    int remoteport = atoi(argv[7]);

    int client, server;
    int master_sock;

    assert(bindaddr);
    assert(bindport > 0);
    assert(localaddr);
    assert(localport > 0);
    assert(remoteaddr);
    assert(remoteport > 0);

    openLogFile("/tmp/tcpproxy");

    signal(SIGINT, cleanup);
    signal(SIGCHLD, sigreap);

    logprintf("Creating server socket %s, %d\n", bindaddr, bindport);
    master_sock = create_server_sock(bindaddr, bindport);
    for (;;)
    {
        if ((client = wait_for_connection(master_sock)) < 0)
        {
            logprintf("Wait for connection returns %d\n", client);
            continue;
        }

        ++nCurrentConnections;
        logprintf("Current connection count %d\n", nCurrentConnections );

        while(nCurrentConnections > nMaxConnections)
        {
            logprintf("PAUSING: Current connection count %d\n", nCurrentConnections );
            //Wait for a signal, which should reduce the connection count
            pause();
            logprintf("UNPAUSED: Current connection count %d\n", nCurrentConnections );
        }

        if (!fork())
        {
            logprintf("connection from %s fd=%d\n", client_hostname, client);
            service_client(client, localurlprefix, localaddr, localport, remoteaddr, remoteport);
        }
        close(client);
        close(server);
    }

}
