#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "dnsrewrite.h"
#include "utils.h"

#include <arpa/inet.h>
#include <linux/types.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nfnetlink_queue.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <errno.h>


//#include <sys/types.h>
//#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <map>

#include "http_get.h"
#include "ResponseTracker.h"
#include "LogHandler.h"
#include "providerDriver.h"


#define CACHE_CATEGORIES
#define LOGLEVEL 4
#define LOG_COMMIT_TIME 30  //Force a save at least every x secs

//Greenteam
#define BLOCKER_IP "81.218.119.11"
//OpenDNS
//#define BLOCKER_IP "146.112.61.106"

#define DNSBLOCKFILE "%s/%s/config/dnsblock"

const char* ADMIN_PORTAL = getenv("ADMIN_PORTAL");
const char* REG_PORTAL   = getenv("REG_PORTAL");
const char* BLOCK_PORTAL = getenv("BLOCK_PORTAL");
const char* HOLD_PORTAL  = getenv("HOLD_PORTAL");
bool bLocalBlock = false;

const char* NET_APPLIANCEIP = getenv("NET_APPLIANCEIP");
const char* STATEFILE = getenv("STATEFILE");
const char* SYSSTATE = getenv("SYSSTATE");
const char* STATE_MONITORED = getenv("STATE_MONITORED");
const char* STATE_UNFILTERED = getenv("STATE_UNFILTERED");
const char* STATE_FILTERED = getenv("STATE_FILTERED");
const char* STATE_BLOCKED = getenv("STATE_BLOCKED");
const char* STATE_REQUESTNAME = getenv("STATE_REQUESTNAME");
const char* STATE_NEEDSAPPROVAL = getenv("STATE_NEEDSAPPROVAL");
const char* STATE_UNREGISTERED = getenv("STATE_UNREGISTERED");

const char* SYSSTATE_CONFIGURED = getenv("SYSSTATE_CONFIGURED");
const char* SYSSTATE_BASICFILTER = getenv("SYSSTATE_BASICFILTER");

const char* DHCPDIR = getenv("DHCPDIR");
const char* DEVICESROOT = getenv("DEVICESROOT");

bool basicFilterMode = true;
bool bConfigured = false;

//const char* SYSSTATE_PRISTINE = getenv("SYSSTATE_PRISTINE");
//const char* SYSSTATE_SUBSCRIBED = getenv("SYSSTATE_SUBSCRIBED");



const char* DNS_UNFILTERED_IP = getenv("DNS_UNFILTERED_IP");
const char* DNS_FILTERED_IP = getenv("DNS_FILTERED_IP");

static LogHandler logHandler;
static LogHandler *pLogHandler = &logHandler;
static providerDriverDNS *dnsProvider = NULL;
static providerDriverCAT *catProvider = NULL;

ResponseTracker rTracker;

bool checkPolicy(const char *ipAddrRequestor, char * classifications, const char *delimeter)
{
    char dnsBlockFile[128];
//TODO: DNSBLOCKFILE should come from environment
    snprintf(dnsBlockFile, sizeof(dnsBlockFile), DNSBLOCKFILE, DHCPDIR, ipAddrRequestor);

    char *saveptr=NULL;
    char * token = strtok_r (classifications, ",", &saveptr);

    while (token != NULL)
    {
        logprintf ("Found token %s\n",token);
        if(find_in_file(dnsBlockFile, token))
        {
            //Blocked
            logprintf("BLOCKED by DNS policy\n");
            return false;
        }
        token = strtok_r (NULL, ",", &saveptr);
    }

    logprintf("NOT BLOCKED by DNS policy\n");
    return true;
}

bool doBlock(const char *devState, const char* hostname, const char *ipAddrAnswer, const char *ipAddrRequestor, LogHandler::logReason *reason, char *szCategory, int nBufSize)
{
    char cachefile[255];
    snprintf(cachefile, sizeof(cachefile), "/tmp/.cache.%s", hostname);

#ifdef CACHE_CATEGORIES
    FILE *fp = fopen(cachefile, "a+");
#else
    FILE *fp = NULL;
#endif
    if(!fp || !fgets(szCategory, nBufSize, fp))
    {
        bool bGotCategory = catProvider->getRemoteCategorization(hostname, szCategory, nBufSize);

        logprintf("Retreived categories are = %s\n", szCategory);
        if(fp)
        {
            if(bGotCategory)
            {
                fputs(szCategory, fp);
            }
            fclose(fp);
        }
    }
    else
    {
        logprintf("CACHED Categories are = %s\n", szCategory);
    }

    if(strlen(szCategory))
    {
        if(strcmp(devState, STATE_MONITORED) == 0)
        {
            //Not blocking - but categorized
            *reason = LogHandler::rMonitored;
        }
        else if(!checkPolicy(ipAddrRequestor, szCategory, ",")) //Check policy returns true if the request should be allowed
        {
            *reason = LogHandler::rOutsidePolicy;
            return true; //Block
        }
        else
        {
            //Ok - this device is ok to access this category of site
            *reason = LogHandler::rWithinPolicy;
        }
    }
    else
    {
        if(strcmp(devState, STATE_MONITORED) == 0)
        {
            //Not blocking - but categorized
            *reason = LogHandler::rMonitored;
        }
        else
        {
            logprintf("Failed to categorize - assuming worst - site is blocked\n");
            *reason = LogHandler::rCategorizationFailed;
            return true;
        }
    }
    //Not blocked
    return false;
}


void setAllARecords(DNSHeader *pDNSHeader, unsigned char *pDNSFirstAnswer, const char* ipAddr)
{
    //Lets find the first A record answer
    uint16_t cnt = ntohs(pDNSHeader->ancount);

    unsigned char *pCur = pDNSFirstAnswer;
    pCur += dns_strlen(pCur) + 1;


    while(cnt)
    {
        DNSAAnswer *pDNSAAnswer = (DNSAAnswer*)pCur;
        logprintf("Examining record of type %d\n", ntohs(pDNSAAnswer->type));

        --cnt;
        if(ntohs(pDNSAAnswer->type) == 1)
        {
            char addrAnswer[20];
            const char *res = inet_ntop(AF_INET, (const void*)&pDNSAAnswer->address, addrAnswer, 20);
            logprintf("Found an A record - current address %s changing to %s\n", addrAnswer, ipAddr);
            inet_aton(ipAddr, &pDNSAAnswer->address);
            pDNSAAnswer->ttl = 0;
        }
        pCur += sizeof(DNSAnswer) + ntohs(pDNSAAnswer->length);
        pCur +=  dns_strlen(pCur) + 1;
    }

}

static int interceptDNSResponse(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data)
{
    //Queue 6 - we can assume is DNS RESPONSE
    //Packet starts at the IPv4 packet
    unsigned char *pIPPacket;

    int payloadLen = nfq_get_payload(nfa, (unsigned char **)&pIPPacket);
    struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfa);
    int id = 0;
    if (ph)
    {
        id = ntohl(ph->packet_id);
    }

    if(payloadLen > 0)
    {
        logprintf("Read %d bytes from nfq\n", payloadLen);
        unsigned char *pUDPPacket, *pDNSPacket, *pDNSQuestion, *pDNSAnswer, *pDNSFirstAnswer;
        DNSHeader *dnsHeader;
        DNSQuestion *dnsQuestion;
        DNSAAnswer *dnsAAnswer;
        char addrRequestor[20];
        uint32_t ipRequestor = ntohl(((iphdr*)pIPPacket)->daddr);

        // The requester is who the DNS response is being sent to
        const char *res = inet_ntop(AF_INET, (const void*)&((iphdr*)pIPPacket)->daddr, addrRequestor, 20);

        char systemState[20];



//      uint16_t ipPacketLen = ntohs(((iphdr*)pIPPacket)->tot_len);
//     logprintf("INITIAL FULL PACKET\n%s\n", dumpBytes((const unsigned char *)pIPPacket, ipPacketLen));

        //What type of response is this - can tell from source IP
        //Is it a response form the local DNSMasq providing filtering or the local one connect to google
        char localDNS[20];
        bool bFilteredResponse = false;
        inet_ntop(AF_INET, (void*)&((iphdr*)pIPPacket)->saddr, localDNS, sizeof(localDNS));

        if(strcmp(localDNS, DNS_FILTERED_IP) == 0)
            bFilteredResponse = true;

        logprintf("Processing response from the %sfiltered interface\n", bFilteredResponse ? "" : "Un");

        //Need to make sure the source address is correct, whatever happens - since the DNS packets have been routed around
        //but the client originally made the request to our primary IP
        inet_pton(AF_INET, NET_APPLIANCEIP, (void*)&((iphdr*)pIPPacket)->saddr);

        if(!setupDNSPacketPointers(pIPPacket, &pUDPPacket, &pDNSPacket, &dnsHeader, &pDNSQuestion, &dnsQuestion, &pDNSAnswer, &dnsAAnswer, &pDNSFirstAnswer))
        {
            logprintf("setupDNSPacketPointers unable to identify packet - Ignoring\n");
        }   //We have a DNS packet, and its an A answer
        else if(!rTracker.onReceiveResponse(ntohl(((iphdr*)pIPPacket)->daddr), dnsHeader, bFilteredResponse))
            //If this returns false, the client has already been sent a response - we should discard
        {
            logprintf("Client already has response\n");
            return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
        }
        //First we should check the system state - regardless of user, if the device is unconfigured we need to take them to the admin portal
        else if(basicFilterMode || bConfigured)
        {
            char szRequestedHost[255];
            dns_strcpy(szRequestedHost, pDNSQuestion);

            //Box is configured.
            //Determine  the filtering state of the user
            //

            //Some things share the same buffer, to save stack space.
            char devStateFile[128];

            char devState[20];

            //We are using devStateFile here - it gets overwritten later
            snprintf(devStateFile, sizeof(devStateFile), "%s/%s", DHCPDIR, addrRequestor);

            //MAC address is link target (assume DHCP'd)
            char macAddr[20]; //Should only be a MAC address + ./
            int sz = sizeof(macAddr);
            if((sz = readlink(devStateFile, macAddr, sz)))
            {
                macAddr[sz]='\0';
                int chop=0;

                if(macAddr[chop] == '.') { ++chop; }
                if(macAddr[chop] == '/') { ++chop; }
                memmove(macAddr, macAddr + chop, sizeof(macAddr) - chop);
            }

            logprintf("DNS request is for %s (%s)\n", addrRequestor, strlen(macAddr) ? macAddr : "NOT DHCPd");


            snprintf(devStateFile, sizeof(devStateFile), "%s/%s/state", DEVICESROOT, macAddr);

            if(basicFilterMode)
            {
                strncpy(devState, STATE_FILTERED, sizeof(devState));
            }
            else if(!get_state(devStateFile, devState, sizeof(devState)))
            {
                strncpy(devState, STATE_UNREGISTERED, sizeof(devState));
            }

            logprintf("Device state determined as : %s. Query was for %s\n", devState, szRequestedHost);

            if(strcmp(devState, STATE_NEEDSAPPROVAL) == 0 || strcmp(devState, STATE_REQUESTNAME) == 0)
            {
                //Unregistered device - direct them to device registration
                logprintf("%s: Sending %s to client %s\n", devState, BLOCK_PORTAL, addrRequestor);
                pLogHandler->logResponse(macAddr, LogHandler::tInternal, ipRequestor, szRequestedHost, LogHandler::rUnregistered, NULL);
                setAllARecords(dnsHeader, pDNSFirstAnswer, REG_PORTAL);
            }
            else if(strcmp(devState, STATE_UNFILTERED) == 0)
            {
                //Parent device, or unfiltered
                logprintf("DEVICE UNFILTERED Sending %s to client %s for request %s\n", inet_ntoa(dnsAAnswer->address), addrRequestor, szRequestedHost);

                //They will ultimately receive the unfiltered response
                pLogHandler->logResponse(macAddr, LogHandler::tUnfiltered, ipRequestor, szRequestedHost, LogHandler::rUnfiltered, NULL);

                //Need to give them the proper address - or not route them at here at all - iptables
                if(bFilteredResponse)
                {
                    //We can return this response, if its not their block page
                    if(dnsProvider->isBlockIP(dnsAAnswer->address))
                    {
                        rTracker.onDiscardFilteredResponse(ntohl(((iphdr*)pIPPacket)->daddr), dnsHeader);

                        //otherwise discard the packet
                        return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
                    }
                }
                //Fall through in the case of the unfiltered response - it will be sent to the client
            }
            else if(strcmp(devState, STATE_BLOCKED) == 0)
            {
                //Blocked device
                logprintf("DEVICE BLOCKED Sending %s to client %s for request %s\n", BLOCK_PORTAL, addrRequestor, szRequestedHost);
                pLogHandler->logResponse(macAddr, LogHandler::tBlock, ipRequestor, szRequestedHost, LogHandler::rDeviceBlocked, NULL);
                setAllARecords(dnsHeader, pDNSFirstAnswer, BLOCK_PORTAL);
            }
            else if(strcmp(devState, STATE_FILTERED) == 0 || strcmp(devState, STATE_MONITORED) == 0 || strcmp(devState, STATE_UNREGISTERED) == 0)
            {
                //Ok - this device is being filtered
                //We are awaiting 2 responses from the DNS *BUT* we can always process the one from the filter service,
                //it is authoritative in the case where we ultimately block, or its not a filtered category

                //
                //
                //logprintf("IPPacket size is %d\n", ((iphdr*)pIPPacket)->ihl * 4);
                //logprintf("UDP Data %d %d %d\n", ntohs(((udphdr*)pUDPPacket)->source), ntohs(((udphdr*)pUDPPacket)->dest), ntohs(((udphdr*)pUDPPacket)->len));
                //logprintf("DNS Request ID = %4.4x\n", ntohs(dnsHeader->id));
                //logprintf("Response flag = %d\n", dnsHeader->qr);
                //logprintf("Query was for %s\n", szRequestedHost);
                //logprintf("Type is %d, Class is %d\n", ntohs(dnsQuestion->type), ntohs(dnsQuestion->cls));
                //logprintf("Answer IP is NOW %s - size is now %d\n", inet_ntoa(dnsAAnswer->address), payloadLen);


                if(bFilteredResponse)
                {
                    //If the IP is the service provider block page - access has been blocked
                    if(dnsProvider->isBlockIP(dnsAAnswer->address))
                    {
                        logprintf("DNS Provider block page (%s)\n", inet_ntoa(dnsAAnswer->address));
                        //We need to categorise and determine whether to block or not
                        char addrAnswer[20];
                        res = inet_ntop(AF_INET, (const void*)&dnsAAnswer->address, addrAnswer, 20);

                        LogHandler::logReason reason = LogHandler::rBasicfilter;

                        char szCategory[128];

                        if(basicFilterMode || strcmp(devState, STATE_UNREGISTERED) == 0 ||
                                doBlock(devState, szRequestedHost, addrAnswer, addrRequestor, &reason, szCategory, sizeof(szCategory)))
                        {
                            pLogHandler->logResponse(macAddr, LogHandler::tBlock, ipRequestor, szRequestedHost, reason, szCategory);
                            if(bLocalBlock)
                            {
                                logprintf("IP BLOCKED Sending %s to client %s for request %s\n", BLOCK_PORTAL, addrRequestor, szRequestedHost);
                                setAllARecords(dnsHeader, pDNSFirstAnswer, BLOCK_PORTAL);
                            }
                            else
                            {
                                //Fall through - we are using the DNS provider block page
                                logprintf("IP BLOCKED Sending %s to client %s for request %s\n", inet_ntoa(dnsAAnswer->address), addrRequestor, szRequestedHost);
                            }
                        }
                        else
                        {
                            //Need to send unfiltered response
                            //Discard this packet
                            //Send the unfiltered response or mark the fact that it should be sent

                            //Whatever happens they are receiving an unfiltered response
                            pLogHandler->logResponse(macAddr, LogHandler::tUnfiltered, ipRequestor, szRequestedHost, reason, szCategory);

                            if(rTracker.onDiscardFilteredResponse(ntohl(((iphdr*)pIPPacket)->daddr), dnsHeader))
                            {
                                //The unfiltered response is available
                                pIPPacket = rTracker.getUnfilteredData(ntohl(((iphdr*)pIPPacket)->daddr), dnsHeader, payloadLen);
                                if(!setupDNSPacketPointers(pIPPacket, &pUDPPacket, &pDNSPacket, &dnsHeader, &pDNSQuestion, &dnsQuestion, &pDNSAnswer, &dnsAAnswer, &pDNSFirstAnswer))
                                {
                                    logprintf("IP ALLOWED FAILED to setupDNSPacketPointers for restored packet\n");
                                }
                                else
                                {
                                    logprintf("IP ALLOWED Sending %s to client %s for request %s\n", inet_ntoa(dnsAAnswer->address), addrRequestor, szRequestedHost);
                                }
                            }
                            else
                            {
                                logprintf("DROPPING FILTERED response %s for request %s (waiting for unfiltered response)\n", inet_ntoa(dnsAAnswer->address), addrRequestor, szRequestedHost);
                                return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
                            }
                        }
                    }
                }
                else
                {
                    //This is the unfiltered response.
                    if(rTracker.needsUnfilteredData(ntohl(((iphdr*)pIPPacket)->daddr), dnsHeader))
                    {
                        //The tracker says the unfiltered data should just go out
                        logprintf("IP ALLOWED (this is a pending unfiltered response) Sending %s to client %s for request %s\n", inet_ntoa(dnsAAnswer->address), addrRequestor, szRequestedHost);
                    }
                    else
                    {
                        //We are not waiting for this data - the response from the filter has probably not arrived yet.
                        //Lets store the data in the tracker
                        //And drop this packet
                        logprintf("Storing UNFILTERED response %s for request %s by client %s\n", inet_ntoa(dnsAAnswer->address), szRequestedHost, addrRequestor);
                        rTracker.onWaitingForFilteredData(ntohl(((iphdr*)pIPPacket)->daddr), dnsHeader, pIPPacket, payloadLen);
                        return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
                    }
                }
            }
            else
            {
                logprintf("No match for device state %s - FROM %s\n", devState, STATE_UNFILTERED);
            }
        }

        //Need to update the checksum
        calcChecksum(pDNSPacket, (payloadLen - (pDNSPacket - pIPPacket)), (struct ip*)pIPPacket, (udphdr*)pUDPPacket);


        int verdictRes = nfq_set_verdict(qh, id, NF_ACCEPT, payloadLen, pIPPacket);
        rTracker.onSendResponse(ntohl(((iphdr*)pIPPacket)->daddr), dnsHeader);

        return verdictRes;
    }

    logprintf("Accepting packet - no change");
    return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
}
void doHousekeeping()
{
    pLogHandler->logCommit(LOG_COMMIT_TIME);
    rTracker.discardUnansweredResponses();
//    pLogHandler->logResponse(macAddr, LogHandler::tInternal, ipRequestor, szRequestedHost, LogHandler::rUnregistered, NULL);
}
//Dont need to be a daemon if using procd
//#define DAEMON
//DROP_PRIV doesnt work!
//#define DROP_PRIV

int main(int argc, char **argv)
{
    struct nfq_handle *h;
    struct nfq_q_handle *qh;
    struct nfnl_handle *nh;
    int fd;
    int rv;
    char buf[4096] __attribute__ ((aligned));

    //Make sure the filrewal rules are set up
    //moved to init.d
    //char cmd[255];
    //snprintf(cmd, sizeof(cmd), "%s/applyRules.sh", getenv("SCRIPTDIR"));
    //system(cmd);

#ifdef DAEMON
    pid_t pid, sid;

    pid = fork();
    if (pid < 0)
    {
        exit(1);
    }
    //Exit parent process
    if (pid > 0)
    {
        if(argc > 1)
        {
            printfile(argv[1], "w", "%d", pid);
        }
        else
        {
            printf("%d", pid);
        }
        exit(0);
    }

    /* Change the file mode mask */
    umask(0);
#endif

    openLogFile("/tmp/dnsrewrite.log");
    setLogLevel(LOGLEVEL);

    logprintf("STARTUP\n");

    //We should just re-exec ourselves with the environment set
    if(!getenv("SYSSTATE_CONFIGURED"))
    {
        logprintf("Env not set\n");
        const char* pszFixEnv=getenv("FIXENV");
        if(!pszFixEnv)
        {
            logprintf("Required environment not set\n");
            exit(1);
        }
        else
        {
            logprintf("Fixing up environment %s %s\n", pszFixEnv, argv[0]);
            if(execl(pszFixEnv, pszFixEnv, argv[0], NULL) == -1)
            {
                logprintf("Exec failed");
            }
        }
    }
    else
    {
        logprintf("Environment ok\n");
    }

#ifdef DAEMON
    sid = setsid();
    if (sid < 0)
    {
        logprintf("Failed to setsid");
        exit(1);
    }

    if ((chdir("/")) < 0)
    {
        logprintf("Failed to chdir");
        exit(1);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
#endif //DAEMON

    basicFilterMode = strcmp(SYSSTATE, SYSSTATE_BASICFILTER) == 0;
    bConfigured = strcmp(SYSSTATE, SYSSTATE_CONFIGURED) == 0;

    dnsProvider = new providerDriverDNS();
    catProvider = providerDriverCAT::getProvider();
    char *pszLocalBlock = getenv("LOCALBLOCK");
    if(pszLocalBlock && strlen(pszLocalBlock))
    {
        bLocalBlock = (pszLocalBlock[0] == '1');
    }
    logprintf("System state is %s\n", SYSSTATE);
    logprintf("DNS provider is %s\n", dnsProvider->getName());
    logprintf("CAT provider is %s\n", catProvider->getName());

    h = nfq_open();
    if (!h)
    {
        logprintf("error during nfq_open()\n");
        exit(1);
    }

    if (nfq_unbind_pf(h, AF_INET) < 0)
    {
        logprintf("error during nfq_unbind_pf()\n");
        exit(1);
    }

    if (nfq_bind_pf(h, AF_INET) < 0)
    {
        logprintf("error during nfq_bind_pf()\n");
        exit(1);
    }

    qh = nfq_create_queue(h,  6, &interceptDNSResponse, NULL);
    if (!qh)
    {
        logprintf("error during nfq_create_queue()\n");
        exit(1);
    }

    if (nfq_set_queue_maxlen(qh, 2048) < 0)
    {
        logprintf("can't set queue length\n");
        exit(1);
    }

    if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0)
    {
        logprintf("can't set packet_copy mode\n");
        exit(1);
    }

    fd = nfq_fd(h);
    int oldfl = fcntl(fd, F_GETFL);
    fcntl(fd, F_SETFL, oldfl & ~O_CLOEXEC);
#ifdef DROP_PRIV
    logprintf("GONNA DROp PRIV\n");
    //Now drop permissions
    int groupid = atoi(getenv("DNSREWRITE_GID"));
    int userid = atoi(getenv("DNSREWRITE_UID"));

    if (getuid() == 0)
    {
        if (setgid(groupid) != 0 || setuid(userid) != 0)
        {
            logprintf("Unable to drop privileges: %s", strerror(errno));
            exit(1);
        }
    }
    logprintf("PRIV dropped\n");
#endif

    logprintf("Starting read loop\n");


    for (;;)
    {
        fd_set read_fd_set;
        FD_ZERO(&read_fd_set);
        FD_SET((unsigned int)fd, &read_fd_set);
        struct timeval timeout;
        timeout.tv_sec = 2;
        timeout.tv_usec = 0;
        
        select(fd+1, &read_fd_set, NULL, NULL, &timeout);

        if(!(FD_ISSET(fd, &read_fd_set)))
        {
            doHousekeeping();
        }
        else if(FD_ISSET(fd, &read_fd_set))
        {
            if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0)
            {
                nfq_handle_packet(h, buf, rv);
                doHousekeeping();
                continue;
            }
            /* if your application is too slow to digest the packets that
             * are sent from kernel-space, the socket buffer that we use
             * to enqueue packets may fill up returning ENOBUFS. Depending
             * on your application, this error may be ignored. Please, see
             * the doxygen documentation of this library on how to improve
             * this situation.
             */
            if (rv < 0 && errno == ENOBUFS)
            {
                logprintf("LOST PACKETS\n");
                continue;
            }
            perror("recv failed");
            break;
        }
    }

    nfq_destroy_queue(qh);

#ifdef INSANE
    /* normally, applications SHOULD NOT issue this command, since
     * it detaches other programs/sockets from AF_INET, too ! */
    logprintf("unbinding from AF_INET\n");
    nfq_unbind_pf(h, AF_INET);
#endif

    nfq_close(h);

    exit(0);

    return 0;
}

static u_int32_t print_pkt (struct nfq_data *tb)
{
    int id = 0;
    struct nfqnl_msg_packet_hdr *ph;
    struct nfqnl_msg_packet_hw *hwph;
    u_int32_t mark,ifi;
    int ret;
    unsigned char *data;

    ph = nfq_get_msg_packet_hdr(tb);
    if (ph)
    {
        id = ntohl(ph->packet_id);
        logprintf("hw_protocol=0x%04x hook=%u id=%u ",
                  ntohs(ph->hw_protocol), ph->hook, id);
    }

    hwph = nfq_get_packet_hw(tb);
    if (hwph)
    {
        int i, hlen = ntohs(hwph->hw_addrlen);

        logprintf("hw_src_addr=");
        for (i = 0; i < hlen-1; i++)
            logprintf("%02x:", hwph->hw_addr[i]);
        logprintf("%02x ", hwph->hw_addr[hlen-1]);
    }

    mark = nfq_get_nfmark(tb);
    if (mark)
        logprintf("mark=%u ", mark);

    ifi = nfq_get_indev(tb);
    if (ifi)
        logprintf("indev=%u ", ifi);

    ifi = nfq_get_outdev(tb);
    if (ifi)
        logprintf("outdev=%u ", ifi);
    ifi = nfq_get_physindev(tb);
    if (ifi)
        logprintf("physindev=%u ", ifi);

    ifi = nfq_get_physoutdev(tb);
    if (ifi)
        logprintf("physoutdev=%u ", ifi);

    ret = nfq_get_payload(tb, &data);
    if (ret >= 0)
        logprintf("payload_len=%d ", ret);

    fputc('\n', stdout);

    return id;
}



