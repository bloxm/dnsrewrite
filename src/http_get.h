#ifndef INC_HTTP_GET_H
#define INC_HTTP_GET_H

#include <arpa/inet.h>

//Get the URL from the IP. Extract the text from the result between matchstart and matchend, and store the result in matchedbuf, null terminated. Return the number of characters in matchedbuf
unsigned int http_get(const char *url, in_addr serverIP, const char *matchstart, const char* matchend, char * matchedbuf, unsigned int bufsize);


#endif //INC_HTTP_GET_H
