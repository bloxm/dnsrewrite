#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include "utils.h"
int create_tcp_socket();

#define HTMLBUFSIZ BUFSIZ
 
unsigned int http_get(const char *url, in_addr serverIP, const char *matchstart, const char* matchend, char * matchedbuf, unsigned int bufsize)
{
  struct sockaddr_in *remote;
  int sock;
  int tmpres;
  const char *get;
  char buf[HTMLBUFSIZ+1];
  char *host;
  char *page;
 
  sock = create_tcp_socket();
  remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in *));
  remote->sin_family = AF_INET;
  remote->sin_addr.s_addr = serverIP.s_addr;
  remote->sin_port = htons(80);
 
  if(connect(sock, (struct sockaddr *)remote, sizeof(struct sockaddr)) < 0){
    perror("Could not connect");
    return 0;
  }
  get = url;
//  fprintf(stderr, "Query is:\n<<START>>\n%s<<END>>\n", get);
 
  //Send the query to the server
  int sent = 0;
  while(sent < strlen(get))
  {
    tmpres = send(sock, get+sent, strlen(get)-sent, 0);
    if(tmpres == -1){
      perror("Can't send query");
      return 0;
    }
    sent += tmpres;
  }
  //now it is time to receive the page
  //
  //This is flawed - we should keep some data from the end of the buffer that might be the beginning of our match string, and then once we have the 
  //beginning of the match string we need to keep it in our buffer
  //
  memset(buf, 0, sizeof(buf));
  int nBufRemain = HTMLBUFSIZ;
  char *pReadIndex = buf;

  int nMatchStart = strlen(matchstart);
  int nMatchEnd = strlen(matchend);


  while((tmpres = recv(sock, pReadIndex, nBufRemain, 0)) > 0)
  {
    char *pBufEnd = pReadIndex + tmpres;
    pBufEnd[1] = '\0';
    char *match1 = (char*)memmem(buf, pBufEnd - buf, matchstart, nMatchStart);

    //logprintf3("searching for %s in %s\n", matchstart, buf);
    if(match1 && (match1 + nMatchStart) < pBufEnd)
    {
        //logprintf3("now searching for %s in %s\n", matchend, match1 + nMatchStart);
        char *match2 = (char*)memmem(match1 + nMatchStart, pBufEnd - (match1 + nMatchStart),  matchend, nMatchEnd);
        if(match2)
        {
            int size = match2 - match1 - nMatchStart;
            if(size < bufsize - 1)
                bufsize = size + 1;

            memcpy(matchedbuf, match1 + nMatchStart, bufsize - 1);
            matchedbuf[bufsize - 1] = '\0';
            logprintf3("Found %s \n", matchedbuf);
            break;
        }
    }

    //If we get here we didnt find all we needed, but may have found something
    //adjust the buffer so we keep the bytes we may care about
    if(match1)
    {
        //We found the start, but not the end
        //Keep all the bytes we have read after match1, and shift them to the beginning of the buffer
        int nKeepBytes = pBufEnd - match1;
        memmove(buf, match1, nKeepBytes);
        pReadIndex = buf + nKeepBytes;
        nBufRemain = HTMLBUFSIZ - nKeepBytes;
    }
    else if((pBufEnd - buf) > nMatchStart)
    {
        //Nothing was found - lets just make sure that we keep at least the number of bytes that 
        //make up our matchstring, in case we have received just the beginning of it
        int nKeepBytes = nMatchStart;
        memmove(buf, pBufEnd - nKeepBytes, nKeepBytes);
        pReadIndex = buf + nKeepBytes;
        nBufRemain = HTMLBUFSIZ - nKeepBytes;
    }
    else //keep all the bytes
    {
        int nKeepBytes = pBufEnd - buf;
        pReadIndex = buf + nKeepBytes;
        nBufRemain = HTMLBUFSIZ - nKeepBytes;
    }
  }
  if(tmpres < 0)
  {
    perror("Error receiving data");
  }
  free(remote);
  close(sock);
  return bufsize;
}
 
int create_tcp_socket()
{
  int sock;
  if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
    perror("Can't create TCP socket");
    exit(1);
  }
  return sock;
}
 
 
