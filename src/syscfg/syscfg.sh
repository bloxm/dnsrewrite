#!/bin/bash

exec > "${IPKG_INSTROOT}/tmp/syscfg.sh"
exec 2>&1
set -x
readlink $0

if [[ "L${OPENWRT_BUILD}" == "L1" ]] ; then
    export ROOT_PREFIX="${IPKG_INSTROOT}"
fi


INSTALL_DIR="bin/bloxm"

INC="./${INSTALL_DIR}/dnsrewrite.inc"
#most important - set the symlink for /dnsrewrite.inc
ln -sf ${INC} ${ROOT_PREFIX}/

. ${ROOT_PREFIX}/dnsrewrite.inc

env

if ! grep dnsrewrite "${ROOT_PREFIX}/etc/passwd" ; then
    #Try to make bash default shell
    #cat ${ROOT_PREFIX}/etc/passwd | grep -v root > ${ROOT_PREFIX}/etc/passwd
    #echo "root:x:0:0:root:/etc/dnsrewrite/scripts:/bin/bash" >> "${ROOT_PREFIX}/etc/passwd"
    echo "dnsrewrite:x:${DNSREWRITE_UID}:${DNSREWRITE_GID}:::/bin/false" >> "${ROOT_PREFIX}/etc/passwd"
    echo "dnsrewrite:x:0:0:99999:7:::" >> "${ROOT_PREFIX}/etc/shadow"
    echo "dnsrewrite:x:${DNSREWRITE_GID}:" >> "${ROOT_PREFIX}/etc/group"
fi

for pol in ${DEFPOLICYDIR}/* ; do 
    ln -s "${pol}" "${POLICYDIR}/$(basename ${pol})" 
done

cp -fv "${SYSCFGDIR}/etc.init.d.dnsmirror" "${ROOT_PREFIX}/etc/init.d/dnsmirror"
cp -fv "${SYSCFGDIR}/etc.init.d.dnsrewrite" "${ROOT_PREFIX}/etc/init.d/dnsrewrite"
cp -fv "${SYSCFGDIR}/etc.init.d.dnsmasq" "${ROOT_PREFIX}/etc/init.d/dnsmasq"
cp -fv "${SYSCFGDIR}/etc.init.d.dnsmasq-local" "${ROOT_PREFIX}/etc/init.d/dnsmasq-local"
cp -fv "${SYSCFGDIR}/etc.init.d.dnsmasq-pairing" "${ROOT_PREFIX}/etc/init.d/dnsmasq-pairing"
cp -fv "${SYSCFGDIR}/etc.init.d.dnsmasq-filtered" "${ROOT_PREFIX}/etc/init.d/dnsmasq-filtered"
cp -fv "${SYSCFGDIR}/etc.init.d.dnsmasq-unfiltered" "${ROOT_PREFIX}/etc/init.d/dnsmasq-unfiltered"
cp -fv "${SYSCFGDIR}/etc.init.d.gpio" "${ROOT_PREFIX}/etc/init.d/gpio"
cp -fv "${SYSCFGDIR}/etc.init.d.checkupdate" "${ROOT_PREFIX}/etc/init.d/checkupdate"
cp -fv "${SYSCFGDIR}/etc.init.d.bloxm" "${ROOT_PREFIX}/etc/init.d/bloxm"
cp -fv "${SYSCFGDIR}/etc.init.d.netpreconf" "${ROOT_PREFIX}/etc/init.d/netpreconf"
cp -fv "${SYSCFGDIR}/etc.init.d.dhcpmonitor" "${ROOT_PREFIX}/etc/init.d/dhcpmonitor"
cp -fv "${SYSCFGDIR}/etc.init.d.emergency" "${ROOT_PREFIX}/etc/init.d/emergency"
cp -fv "${SYSCFGDIR}/etc.init.d.systemstate" "${ROOT_PREFIX}/etc/init.d/systemstate"
cp -fv "${SYSCFGDIR}/etc.init.d.goahead" "${ROOT_PREFIX}/etc/init.d/goahead"
cp -fv "${SYSCFGDIR}/etc.init.d.tcpproxy" "${ROOT_PREFIX}/etc/init.d/tcpproxy"
cp -fv "${SYSCFGDIR}/etc.init.d.configchroot" "${ROOT_PREFIX}/etc/init.d/configchroot"

#cp -fv "${SYSCFGDIR}/etc.dnsmasq.local.conf" "${ROOT_PREFIX}/etc/dnsmasq.local.conf"
#cp -fv "${SYSCFGDIR}/etc.dnsmasq.local.pristine.conf" "${ROOT_PREFIX}/etc/dnsmasq.local.pristine.conf"
#cp -fv "${SYSCFGDIR}/etc.dnsmasq.filtered.conf" "${ROOT_PREFIX}/etc/dnsmasq.filtered.conf"
#cp -fv "${SYSCFGDIR}/etc.dnsmasq.unfiltered.conf" "${ROOT_PREFIX}/etc/dnsmasq.unfiltered.conf"

cp -fv "${SYSCFGDIR}/etc.hosts" "${ROOT_PREFIX}/etc/hosts"
cp -fv "${SYSCFGDIR}/etc.udhcpc.user" "${ROOT_PREFIX}/etc/udhcpc.user"
#cp -fv "${SYSCFGDIR}/etc.config.dhcp" "${ROOT_PREFIX}/etc/config/dhcp"


#cp -fv "${SYSCFGDIR}/etc.resolv.upstream" "${ROOT_PREFIX}/etc/resolv.upstream"
#cp -fv "${SYSCFGDIR}/etc.resolv.filtered" "${ROOT_PREFIX}/etc/resolv.filtered"
#cp -fv "${SYSCFGDIR}/etc.resolv.unfiltered" "${ROOT_PREFIX}/etc/resolv.unfiltered"
cp -fv "${SYSCFGDIR}/etc.rc.button.BTN_0" "${ROOT_PREFIX}/etc/rc.button/BTN_0"

if [[ ! -L ${ROOT_PREFIX}/etc/config/network ]] ; then
    rm -f ${ROOT_PREFIX}/etc/config/network
    ln -s /tmp/netconfig ${ROOT_PREFIX}/etc/config/network
fi

#if [[ $(md5sum "${SYSCFGDIR}/etc.config.wireless" | awk '{print $1}') != $(md5sum "${ROOT_PREFIX}/etc/config/wireless" | awk '{print $1}') ]] ; then
#    cp -fv "${SYSCFGDIR}/etc.config.wireless" "${ROOT_PREFIX}/etc/config/wireless"
#    [[ "L${OPENWRT_BUILD}" == "L1" ]] || uci commit wireless
#    [[ "L${OPENWRT_BUILD}" == "L1" ]] || /etc/init.d/network restart
    #Network restart clears the firewall rules
#    [[ "L${OPENWRT_BUILD}" == "L1" ]] || ${ROOT_PREFIX}/${SCRIPTDIR}/applyRules.sh
#fi

if [[ $(md5sum "${SYSCFGDIR}/etc.config.system" | awk '{print $1}') != $(md5sum "${ROOT_PREFIX}/etc/config/system" | awk '{print $1}') ]] ; then
    cp -fv "${SYSCFGDIR}/etc.config.system" "${ROOT_PREFIX}/etc/config/system"
    [[ "L${OPENWRT_BUILD}" == "L1" ]] || uci commit system
    [[ "L${OPENWRT_BUILD}" == "L1" ]] || /etc/init.d/led restart
fi

#[[ "L${OPENWRT_BUILD}" == "L1" ]] || /etc/init.d/dnsmasq restart
if [[ "L${OPENWRT_BUILD}" != "L1" ]] ; then
    #Ensure correct ownership - this is done in /etc/init.d/bloxm 
    /etc/init.d/bloxm restart
fi
