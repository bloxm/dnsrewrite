#ifndef INC_DNSREWRITE_H
#define INC_DNSREWRITE_H


#include <netinet/in.h>
#include <netinet/udp.h>
#include <netinet/ip.h>
 
struct DNSHeader{
        unsigned        id :16;         /* query identification number */
        unsigned        qr: 1;          /* response flag */
        unsigned        opcode: 4;      /* purpose of message */
        unsigned        aa: 1;          /* authoritive answer */
        unsigned        tc: 1;          /* truncated message */
        unsigned        rd: 1;          /* recursion desired */
        unsigned        ra: 1;          /* recursion available */
        unsigned        unused :3;      /* unused bits (MBZ as of 4.9.3a3) */
        unsigned        rcode :4;       /* response code */
        unsigned        qdcount :16;    /* number of question entries */
        unsigned        ancount :16;    /* number of answer entries */
        unsigned        nscount :16;    /* number of authority entries */
        unsigned        arcount :16;    /* number of resource entries */
} __attribute__ ((packed));

struct DNSQuestion
{
  uint16_t  type;
  uint16_t  cls;
}__attribute__ ((packed));

struct DNSAnswer
{
  uint16_t  type;
  uint16_t  cls;
  uint32_t  ttl;
  uint16_t  length;
} __attribute__((packed));

struct DNSAAnswer
{
  uint16_t  type;
  uint16_t  cls;
  uint32_t  ttl;
  uint16_t  length;
  in_addr   address;
}__attribute__ ((packed));

#endif //INC_DNSREWRITE_H
