#ifndef IPPKT_HPP
#define IPPKT_HPP

#include <netinet/udp.h>
#include <netinet/ip.h>


struct ippkt 
{
    struct ip ip;
    struct udphdr udp;


    static void calcChecksum(const void *payload, size_t len, struct ip * ip_, struct udphdr * udp_)
    {
	ip_->ip_sum = 0;
	udp_->check = 0;

	ip_->ip_sum = wrapsum(checksum(ip_, sizeof(*ip_), 0));
	udp_->check = wrapsum(
		      checksum(
			udp_, sizeof(*udp_),
			checksum(
			    payload, len,
				checksum(
				    &ip_->ip_src,
				    2 * sizeof(ip_->ip_src),
				    IPPROTO_UDP + (uint32_t)ntohs(udp_->len)
		)
	    )
	));
    }
    void calcChecksum(const void *payload, size_t len)
    {
	ippkt::calcChecksum(payload, len, &ip, &udp);
    }


private:
    static uint32_t checksum(const void *buf, size_t len, uint32_t sum) 
    {
	    const char *p = reinterpret_cast<const char*>(buf);
	    size_t i;

        for (i = 0; i < (len & ~1); i += 2) {
            sum += ntohs(*(uint16_t *)(p + i));
            if (sum > 0xffff)
                sum -= 0xffff;
        }

        if (i < len) {
            uint16_t val = p[i];
            val <<= 8;
            sum += val;
            if (sum > 0xffff)
                sum -= 0xffff;
        }

        return sum;
    }

    static uint32_t wrapsum(uint32_t sum) 
    {
	sum = ~sum & 0xffff;
	return htons(sum);
    }
} __attribute__ ((packed));


//#define ETH_HEADER



struct rawDNSRequest
{
#ifdef ETH_HEADER
    ether_header m_ethHeader;
#endif
    ippkt m_ippkt;
    unsigned char *m_packetData;
    rawDNSRequest() : m_packetData(NULL)
    {
#ifdef ETH_HEADER
        memset(&m_ethHeader, 0, sizeof(m_ethHeader));
#endif
        memset(&m_ippkt, 0, sizeof(m_ippkt));
    }
    ~rawDNSRequest()
    { if(m_packetData) delete [] m_packetData; }

    //Return the bytes a packet, and its length - memory is owned by this object
    virtual const unsigned char *makePacketData(const unsigned char *udpPayload, 
                                                uint16_t & nLength_, 
                                                uint16_t sourcePort, 
                                                uint16_t destPort, 
                                                in_addr sourceIP, 
                                                in_addr destIP)
    {
        makeRaw(nLength_, sourcePort, destPort, sourceIP, destIP);

        if(m_packetData)
        {
            delete [] m_packetData;
        }


#ifdef ETH_HEADER
        uint16_t buflen = sizeof(m_ethHeader) + sizeof(m_ippkt) + nLength_;
        m_packetData = new unsigned char[buflen];

        memcpy(m_packetData, &m_ethHeader, sizeof(m_ethHeader));
        memcpy(m_packetData+sizeof(m_ethHeader), &m_ippkt, sizeof(m_ippkt));
        memcpy(m_packetData+sizeof(m_ippkt)+sizeof(m_ethHeader), udpPayload, nLength_);
        
        //Ensure there is enough data for the checksum
        m_ippkt.calcChecksum(m_packetData+sizeof(m_ippkt)+sizeof(m_ethHeader), nLength_);

        //Rewrite the header, with the checksum included
        memcpy(m_packetData+sizeof(m_ethHeader), &m_ippkt, sizeof(m_ippkt));

        nLength_ += (sizeof(m_ethHeader) + sizeof(m_ippkt));
#else
        uint16_t buflen = sizeof(m_ippkt) + nLength_;
        m_packetData = new unsigned char[buflen];

        memcpy(m_packetData, &m_ippkt, sizeof(m_ippkt));
        memcpy(m_packetData+sizeof(m_ippkt), udpPayload, nLength_);
        //
        //Ensure there is enough data for the checksum
        m_ippkt.calcChecksum(m_packetData+sizeof(m_ippkt), nLength_);

        //Rewrite the header, with the checksum included
        memcpy(m_packetData, &m_ippkt, sizeof(m_ippkt));

        nLength_ += sizeof(m_ippkt);
#endif

        return m_packetData;
    }
    virtual void makeRaw(uint16_t nDataLength_, uint16_t sourcePort, uint16_t destPort, in_addr sourceIP, in_addr destIP)
    {
        m_ippkt.ip.ip_v = 4;
        m_ippkt.ip.ip_hl = 5;
        m_ippkt.ip.ip_tos = IPTOS_LOWDELAY;
        m_ippkt.ip.ip_len = htons(sizeof(m_ippkt) + nDataLength_);
        m_ippkt.ip.ip_id = 1;
        m_ippkt.ip.ip_off = 0;
        m_ippkt.ip.ip_ttl = 16;
        m_ippkt.ip.ip_p = IPPROTO_UDP;
        m_ippkt.ip.ip_sum = 0;
        m_ippkt.udp.len = htons(sizeof(struct udphdr) + nDataLength_);
        m_ippkt.udp.check = 0;
        m_ippkt.ip.ip_src = sourceIP;
        m_ippkt.ip.ip_dst = destIP;
        m_ippkt.udp.source = htons(sourcePort);
        m_ippkt.udp.dest = htons(destPort);

    }
};



#endif // IPPKT_HPP

