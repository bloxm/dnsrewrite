
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include "providerDriver.h"
#include "http_get.h"

bool providerDriverCAT::getCategoryFromBlockPage(const char *hostName, 
                                     const char *matchstart, 
                                     const char *matchend,
                                     char *szCategory,
                                     const int nBufSize)
{
    in_addr host;
    const char *request = getHTTPRequest(hostName, &host);
    if(request)
    {
        unsigned int bufsize = http_get(request, host, matchstart, matchend, szCategory, nBufSize);
        szCategory[bufsize] = 0;

        return bufsize > 0;
    }
    return false;
}


providerDriverDNS::providerDriverDNS()
{
    const char *env=getenv("DNS_PROVIDER");
    char szPath[256];
    snprintf(szPath, sizeof(szPath), "%s/%s/.blockips", getenv("DNSPROVDIR"), env);
    FILE *fp = fopen(szPath, "r");
    if(fp)
    {
        nBlockIPs=0;
        char *ret;
        char buf[20];

        while((ret = fgets(buf, sizeof(buf), fp)) != NULL && nBlockIPs < sizeof(blockIPs))
        {
            //Remove the newline
            int nNL = strlen(buf) - 1;
            if(buf[nNL] == '\n')
            {
                buf[nNL] = 0;
            }
            inet_aton(buf, &blockIPs[nBlockIPs++]);
        }
        fclose(fp);
    }
}

bool providerDriverDNS::isBlockIP(in_addr resolvedIP)
{
    for(int i=0; i < nBlockIPs; i++)
    {
        if(resolvedIP.s_addr == blockIPs[i].s_addr)
            return true;
    }
    return false;
}


providerDriverCAT *providerDriverCAT::getProvider()
{
    const char *env=getenv("CAT_PROVIDER");

    if(env)
    {
        if(strcmp(env, "greenteam") == 0)
        {
            return new greenTeamCAT();
        }
        else if(strcmp(env, "fortiguard") == 0)
        {
            return new fortiGuardCAT();
        }
    }
    return NULL;
}

bool providerDriverCAT::getHostIP(const char * hostname , in_addr *ip)
{
    struct hostent *he;
    struct in_addr **addr_list;
    int i;
         
    if ((he = gethostbyname(hostname)) == NULL) 
    {
        return false;
    }
 
    addr_list = (struct in_addr **) he->h_addr_list;
     
    for(i = 0; addr_list[i] != NULL; i++) 
    {
        //Return the first one;
        *ip = *addr_list[i];
        return true;
    }
     
    return false;
}
