#ifndef __LOGHANDLER_HPP__
#define __LOGHANDLER_HPP__

#include <string.h>
#include <stdint.h>
#include <time.h>

#include <map>
#include <list>
#include <string>
#include "utils.h"

//Additional 3 bytes are added below for the '}', '\n' and \0
#define LOG_ENTRY_SIZE 252

class LogHandler
{
    public:
        enum logReason
        {
            rUnregistered=0,
            rUnfiltered,
            rBasicfilter,
            rWithinPolicy,
            rOutsidePolicy,
            rAwaitingParentAuth,
            rDeviceBlocked,
            rMonitored,
            rCategorizationFailed
        };
        enum logEntryType
        {
            tBlock,
            tUnfiltered,
            tInternal
        };
        struct logEntry
        {
            char buf[LOG_ENTRY_SIZE+3];
            unsigned short bufCnt; //Number of bytes written
            bool bDataAdded;
            uint32_t logTime;
            logEntry() : bufCnt(0), bDataAdded(false){}

            void openEntry();
            void addData(const char *key, uint32_t value);
            void addData(const char *key, int32_t value);
            void addData(const char *key, const char *value);
            void closeEntry();
        };

        typedef std::list<logEntry*> logList;

        static const int maxQueueSize = 10;
        static const int secsLogDuplicate = 60;
        struct logFile
        {
            FILE *fp;
            std::string macAddr;
            logList queue; //Items committed to the log
            logEntry *pLastItem; //Last item, will be added if the next one is the different, or the log is comitted
            bool bLastItemSaved;
            uint32_t nLineCount;
            time_t tLastSave;

            logFile(const char *_macAddr) : fp(NULL), pLastItem(NULL), bLastItemSaved(false), macAddr(_macAddr), nLineCount(0), tLastSave(time(NULL))
            {
            }

            void commit()
            {
                logprintf5("commit\n");
                if(pLastItem)
                {
                    logprintf5("commit has last item \n");
                    //save may have previously commited that entry to disk,
                    //but left it here for comparison
                    if(bLastItemSaved)
                    {
                    logprintf5("commit dealing with lastItem saved\n");
                        bLastItemSaved = false;
                        delete pLastItem;
                    }
                    else
                    {
                    logprintf5("Queuing last item\n");
                        queue.push_back(pLastItem);
                    }
                    pLastItem = NULL;
                }
                //Check the queue size
                if(queue.size() >= maxQueueSize)
                {
                    logprintf5("Queuing size is %d - saving\n", queue.size());
                    _save();
                }
            }
            void save(int logCommitMinFreq = 0)
            {
                //We will save the last item so it is in the logfile asap, 
                //we mark it such, so we can still avoid duplicates
                if(pLastItem && !bLastItemSaved)
                {
                    logEntry *pNew = new logEntry();
                    *pNew = *pLastItem;
                    queue.push_back(pNew);
                    //We have saved the item but not removing it
                    bLastItemSaved = true;
                }
                time_t tNow = time(NULL);
                if(tNow >= tLastSave + logCommitMinFreq)
                {
                    _save(tNow);
                }
            }

            void _save(time_t tNow=time(NULL));
            bool _open();
            bool _close();
            void add(logEntry *pNew)
            {
                logprintf5("add\n");
                if(pLastItem)
                {
                    logprintf5("add - there is a last item\n");
                    //We are checking for equality of the buffer size, and also whether the log entries are far enough apart
                    //it will have to be logged anyway
                    if(pNew->bufCnt == pLastItem->bufCnt)
                    {
                        logprintf5("add - passes first test\n");
                        logprintf5("New Time %d. Last Time %d. Time apart %d\n", pNew->logTime, pLastItem->logTime, pNew->logTime - pLastItem->logTime);
                        int nTimeDiff = pNew->logTime - pLastItem->logTime;
                        //Now checking whether the previous log item has been there so long, that we will log this one anyway
                        if(nTimeDiff < secsLogDuplicate)
                        {
                            logprintf5("add - passes time test\n");
                            //Check if these are the same
                            if(strncmp(pNew->buf, pLastItem->buf, pNew->bufCnt) == 0)
                            {
                                logprintf5("Same....\n");
                                delete pNew;
                                return;
                            }
                        }
                    }

                    logprintf5("Different message - asking for commit\n");
                    //put pLast item into the queue, and potentially save the file
                    commit();
                }
                logprintf5("Setting last item\n");

                pLastItem = pNew;
            }
        };

        //Map log file to list of entries waiting to be written
        typedef std::map<std::string, logFile*> logMap;
        logMap logFiles;
    protected:
        void addLogEntry(const char * macAddr, logEntry *pLogEntry)
        {
            logMap::iterator itFound = logFiles.find(macAddr);
            logFile *pLogFile = NULL;
            if(itFound == logFiles.end())
            {
                pLogFile = new logFile(macAddr);
                logFiles.insert(logMap::value_type(macAddr, pLogFile));
            }
            else
            {
                pLogFile = itFound->second;
            }
            logprintf5("Adding a new log entry \n");
            pLogFile->add(pLogEntry);
        }

    public:
        void logResponse(const char *macAddr, logEntryType type, uint32_t ipRequestor, const char *szRequestedHost, logReason reason, const char *szCategory);
        //Ensure all logs are committed to disk, at least this often
        void logCommit(int logCommitMinFreq);
};


#endif
