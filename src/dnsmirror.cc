#include<stdio.h> //printf
#include<string.h> //memset
#include<stdlib.h> //exit(0);
#include<arpa/inet.h>
#include<sys/socket.h>
#include<unistd.h>

#include<linux/if_ether.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <sys/select.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <errno.h>

#include "ippkt.hpp"
#include "utils.h"

#include <linux/if_tun.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "pthread.h"

#define BUFLEN 512  //Max length of buffer
#define PORT 54   //The port on which to listen for incoming data

#define LOGLEVEL 4


//Dont need to be a daemon if using procd
//#define DAEMON

//DROP_PRIV never tested
//#define DROP_PRIV
 
void die(const char *s, int err = errno)
{
    logprintf(strerror(err));
    exit(1);
}

struct rwInfo
{
    int read_fd;
    int write_fd;
};

rwInfo rw0, rw1;

void * rwThread(void *arg)
{
    rwInfo *pInfo = (rwInfo*)arg;
    
    char buf[1550];
    int rx;
    logprintf("Thread %d -> %d\n", pInfo->read_fd,pInfo->write_fd);

    while(1)
    {
        rx=read(pInfo->read_fd, buf, sizeof(buf));
//        logprintf("Read %d from %d\n", rx, pInfo->read_fd);
        if(write(pInfo->write_fd, buf, rx) != rx)
        {
            logprintf("%d -> %d failed to write all bytes\n", pInfo->read_fd,pInfo->write_fd);
        }
    }
    return NULL;
}

void tapRunner()
{
    logprintf("tapRunner\n");
    
    struct ifreq ifr;
    int err;
    int tap0;
    int tap1;

    if( (tap0 = open("/dev/net/tun", O_RDWR)) < 0 )
    {
        die("Failed to open tap0 /dev/net/tun");
    }

    memset(&ifr, 0, sizeof(ifr));
    ifr.ifr_flags = IFF_TAP;
    strncpy(ifr.ifr_name, "tap0", IFNAMSIZ);

    if( (err = ioctl(tap0, TUNSETIFF, (void *) &ifr)) < 0 )
    {
        close(tap0);
        die("failed to ioctl(tap0)\n", err);
    }

    if( (tap1 = open("/dev/net/tun", O_RDWR)) < 0 )
    {
        die("Failed to open tap1 /dev/net/tun\n");
    }

    memset(&ifr, 0, sizeof(ifr));
    ifr.ifr_flags = IFF_TAP;
    strncpy(ifr.ifr_name, "tap1", IFNAMSIZ);

    if( (err = ioctl(tap1, TUNSETIFF, (void *) &ifr)) < 0 )
    {
        close(tap1);
        die("failed to ioctl(tap1)\n", err);
    }
    logprintf("Tap FDs %d, %d\n", tap0, tap1);

    usleep(2500);

    pthread_t tap0Thread, tap1Thread;

    rw0.read_fd=tap0;
    rw0.write_fd=tap1;
    rw1.read_fd=tap1;
    rw1.write_fd=tap0;

    pthread_attr_t thread_attr;
    if (pthread_attr_init(&thread_attr) != 0)
    {
        die("Failed to pthread_attr_init\n");
    }
    if (pthread_attr_setstacksize(&thread_attr, PTHREAD_STACK_MIN)!= 0) 
    {
        die("Failed pthread_attr_setstacksize\n");
        exit(1);
    }
    logprintf("Starting thread for %d -> %d\n", rw0.read_fd, rw0.write_fd); 
    if(pthread_create(&tap0Thread, &thread_attr, rwThread, &rw0))
    {
        die("Failed pthread_create(0)\n");
        exit(1);
    }

    logprintf("Starting thread for %d -> %d\n", rw1.read_fd, rw1.write_fd); 
    if(pthread_create(&tap1Thread, &thread_attr, rwThread, &rw1))
    {
        die("Failed pthread_create(1)\n");
        exit(1);
    }

    /*
     * Need to issue these commands
     * ifconfig tap0 0.0.0.0 up
     * brctl addif br-lan tap0
     * ifconfig tap1 hw ether ${TAP_MAC_ADDRESS}
     * ifconfig tap1 ${TAP_IP_ADDRESS} up
     */
    system("/sbin/ifconfig tap0 0.0.0.0 up");
    system("/usr/sbin/brctl addif br-lan tap0");
    char cmd[255];
    snprintf(cmd, sizeof(cmd), "/sbin/ifconfig tap1 hw ether %s", getenv("TAP_MAC_ADDRESS"));
    system(cmd);
    snprintf(cmd, sizeof(cmd), "/sbin/ifconfig tap1 %s up", getenv("TAP_IP_ADDRESS"));
    system(cmd);
}

int main(int argc, char**argv)
{
    
#ifdef DAEMON
    pid_t pid, sid;

    pid = fork();
    if (pid < 0) 
    {
         exit(1);
    }
    //Exit parent process
    if (pid > 0) 
    {
        if(argc > 1)
        {
            printfile(argv[1], "w", "%d", pid);
        }
        else
        {
            printf("%d", pid);
        }
        exit(0);
    }

    /* Change the file mode mask */
    umask(0);
#endif

    openLogFile("/tmp/dnsmirror.log");
    setLogLevel(LOGLEVEL);

    logprintf("STARTUP\n");

    //We should just re-exec ourselves with the environment set
    if(!getenv("SYSSTATE_CONFIGURED"))
    {
//        logprintf("Env not set\n");
        const char* pszFixEnv=getenv("FIXENV");
        if(!pszFixEnv)
        {
            die("Required environment not set\n");
        }
        else
        {
//            logprintf("Fixing up environment %s %s\n", pszFixEnv, argv[0]);
            execl(pszFixEnv, pszFixEnv, argv[0], NULL);
        }
    }
    else
    {
            logprintf("Environment ok\n");
    }

#ifdef DAEMON    
    sid = setsid();
    if (sid < 0) 
    {
        logprintf("Failed to setsid");
        exit(1);
    }

    if ((chdir("/")) < 0) 
    {
        logprintf("Failed to chdir");
        exit(1);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
#endif //DAEMON

    tapRunner();



    struct sockaddr_in si_me, si_other;
     
    int s;
    socklen_t slen = sizeof(si_other) , recv_len;
    char buf[BUFLEN];
     
    //create a raw socket
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        die("socket");
    }


    sockaddr_in bindInfo;
    memset(&bindInfo, 0, sizeof(bindInfo));
    bindInfo.sin_family = AF_INET;
    bindInfo.sin_port = htons(53);
    logprintf("Binding to %s\n", getenv("NET_APPLIANCEIP"));
    inet_aton(getenv("NET_APPLIANCEIP"), &bindInfo.sin_addr);

    if(bind(s, (sockaddr*)&bindInfo, sizeof(bindInfo)) != 0)
    {
        logprintf("Bind failed for listensocket %s", strerror(errno));
        close(s);
        die("bind");
    }

    //now listening
   
    //Create the socket for sending the duplicated packets
    int rawSock = socket(AF_PACKET, SOCK_DGRAM, htons(ETH_P_IP));


#ifdef DROP_PRIV
    logprintf("GONNA DROp PRIV\n");
    //Now drop permissions
    int groupid = atoi(getenv("DNSREWRITE_GID"));
    int userid = atoi(getenv("DNSREWRITE_UID"));

    if (getuid() == 0)
    {
        if (setgid(groupid) != 0 || setuid(userid) != 0)
        {
            logprintf("Unable to drop privileges: %s", strerror(errno));
            exit(1);
        }
    }
    logprintf("PRIV dropped\n");
#endif



    struct ifreq LANInterfaceInfo;      
    
    //strcpy( interfaceInfo.ifr_name, tunName );
    strcpy( LANInterfaceInfo.ifr_name, "br-lan" );
    if(ioctl(rawSock, SIOCGIFINDEX, (char*)&LANInterfaceInfo ) == -1)
    {
        die("Failed to get LAN Interface info");
    }

    //0x0A makes it reserved
    unsigned char ether_addr[6];
    sscanf(getenv("TAP_MAC_ADDRESS"), "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &ether_addr[0], &ether_addr[1], &ether_addr[2], &ether_addr[3], &ether_addr[4], &ether_addr[5]);

    rawDNSRequest dup;

    const char* DNS_UNFILTERED_IP = getenv("DNS_UNFILTERED_IP");
    const char* DNS_FILTERED_IP = getenv("DNS_FILTERED_IP");

    uint32_t nPacketCount=0;
    const uint32_t nLogSkip=10;

    logprintf("starting listen loop\n");
    //keep listening for data
    while(1)
    {
        unsigned char dataGramPacket[ETH_FRAME_LEN];
        sockaddr_in sourceAddress;

        socklen_t addrSize = sizeof(sourceAddress);
        int bytesRead = recvfrom(s, dataGramPacket, sizeof(dataGramPacket), 0, (struct sockaddr *) &sourceAddress, &addrSize);
        logprintf5("Read %d bytes\n", bytesRead);

        if(bytesRead)
        {
            if(nPacketCount % nLogSkip == 0)
            {
               logprintf4("Packet from %s:%d. Received %d packets. Logging every %d.\n", inet_ntoa(sourceAddress.sin_addr),  ntohs(sourceAddress.sin_port), nPacketCount, nLogSkip);
            }
            nPacketCount++;
           //logprintf("Port %d\n", ntohs(sourceAddress.sin_port));
//           logprintf("BYTES : %d\n%s\n", bytesRead, dumpBytes(dataGramPacket, bytesRead));

            in_addr srv1, srv2;
            inet_aton(DNS_UNFILTERED_IP, &srv1);
            inet_aton(DNS_FILTERED_IP, &srv2);



            {
                uint16_t nBytesToWrite = bytesRead;
                const unsigned char *pData = dup.makePacketData(dataGramPacket, nBytesToWrite, ntohs(sourceAddress.sin_port), 53, sourceAddress.sin_addr, srv1);
                struct sockaddr_ll addr={0};
                addr.sll_family=AF_PACKET;
                addr.sll_ifindex=LANInterfaceInfo.ifr_ifindex;  //Packet needs to leave from this interface
                addr.sll_halen=ETHER_ADDR_LEN;
                addr.sll_protocol=htons(ETH_P_IP);

                memcpy(&addr.sll_addr, ether_addr, ETHER_ADDR_LEN);

//               logprintf("WRITING BYTES : %d\n%s\n", nBytesToWrite, dumpBytes(pData, nBytesToWrite));
                if (sendto(rawSock, pData, nBytesToWrite, 0, (struct sockaddr*) &addr, sizeof(addr)) == -1)
                {
                    die("sendto1()");
                }
            }

            {
                uint16_t nBytesToWrite = bytesRead;
                const unsigned char *pData = dup.makePacketData(dataGramPacket, nBytesToWrite, ntohs(sourceAddress.sin_port), 53, sourceAddress.sin_addr, srv2);
                struct sockaddr_ll addr={0};
                addr.sll_family=AF_PACKET;
                addr.sll_ifindex=LANInterfaceInfo.ifr_ifindex;  //Packet needs to leave from this interface
                addr.sll_halen=ETHER_ADDR_LEN;
                addr.sll_protocol=htons(ETH_P_IP);
                
                memcpy(&addr.sll_addr, ether_addr, ETHER_ADDR_LEN);

               logprintf5("WRITING BYTES : %d\n%s\n", nBytesToWrite, dumpBytes(pData, nBytesToWrite));

                if (sendto(rawSock, pData, nBytesToWrite, 0, (struct sockaddr*) &addr, sizeof(addr)) == -1)
                {
                    die("sendto2()");
                }
            }
        }
    }
 
    close(s);
    return 0;
}
