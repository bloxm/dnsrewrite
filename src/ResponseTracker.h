#ifndef __INC_RESPONSETRACKER_H
#define __INC_RESPONSETRACKER_H

#include <map>
#include <time.h>
#define RESPONSE_TIMEOUT 15

class ResponseTracker
{
private:
    enum respState { rpReceived=0, rpAlreadySent = 1, rpNeedsUnfilteredData = 2, rpHasUnfilteredData = 3};
public:
    bool discardUnansweredResponses()
    {
        RMAP::iterator itCur = m_responseMap.begin();
        RMAP::iterator itEnd = m_responseMap.end();

        time_t tNow = time(NULL);

        while(itCur != itEnd)
        {
            RMAP::iterator itTemp = itCur++;
            if(tNow >= itTemp->second.time + RESPONSE_TIMEOUT)
            {
                logprintf("Discarding aged out response %d, %d\n", itTemp->first.k1, itTemp->first.k2);

                if(itTemp->second.packet)
                {
                    delete [] itTemp->second.packet;
                    itTemp->second.packet = NULL;
                }
                m_responseMap.erase(itTemp);
            }
        }
    }
    //Receiving something from DNS
    //Keep track - if something has already been sent for this client, return false, and delete the entry
    bool onReceiveResponse(uint32_t requestingIP, DNSHeader *pdnsHeader, bool bFiltered)
    {
        printf("onReceiveResponse(%d, %d)\n", requestingIP, ntohs(pdnsHeader->id));
        key k{requestingIP, ntohs(pdnsHeader->id)};

        RMAP::iterator itFound = m_responseMap.find(k);
        RMAP::iterator itEnd = m_responseMap.end();

        if(itFound != itEnd)
        {
            if(itFound->second.state == rpAlreadySent)
            {
                if(itFound->second.packet)
                {
                    delete [] itFound->second.packet;
                    itFound->second.packet = NULL;
                }
                m_responseMap.erase(itFound);
                printf("Already sent\n");
                return false;
            }
            if(bFiltered && itFound->second.filtered)
            {
                //Ignoring duplicate packet
                printf("Ignoring duplicate filtered packet\n");
                return false;
            }
            if(!bFiltered && itFound->second.unfiltered)
            {
                //Ignoring duplicate packet
                printf("Ignoring duplicate UNfiltered packet\n");
                return false;
            }
        }
        else
        {
            printf("NEW ENTRY\n");
            data d;
            m_responseMap[k] = d;
        }
        return true;
    }
    //When a request has been sent, mark it as such
    void onSendResponse(uint32_t requestingIP, DNSHeader *pdnsHeader)
    {
        printf("onSendResponse(%d, %d)\n", requestingIP, ntohs(pdnsHeader->id));
        key k{requestingIP, ntohs(pdnsHeader->id)};

        RMAP::iterator itFound = m_responseMap.find(k);
        RMAP::iterator itEnd = m_responseMap.end();

        if(itFound != itEnd)
        {
            //If we are sending a packet, and we were holding on to unfiltered data
            //we can conclude that both requests are since done
            if(itFound->second.state == rpHasUnfilteredData || itFound->second.state == rpNeedsUnfilteredData)
            {
                if(itFound->second.packet)
                {
                    printf("ALL_SORTED onSendResponse(%d, %d)\n", requestingIP, ntohs(pdnsHeader->id));
                    delete [] itFound->second.packet;
                    itFound->second.packet = NULL;
                }
                m_responseMap.erase(itFound);
            }
            else
            {
                printf("MARKING ALREADY_SENT onSendResponse(%d, %d)\n", requestingIP, ntohs(pdnsHeader->id));
                itFound->second.state = rpAlreadySent;
            }
        }
    }
    //If we are throwing away the filtered response
    //Mark it as such
    //Return true if the unfiltered data is already available
    bool onDiscardFilteredResponse(uint32_t requestingIP, DNSHeader *pdnsHeader)
    {
        printf("onDiscardFiltered(%d, %d)\n", requestingIP, ntohs(pdnsHeader->id));
        key k{requestingIP, ntohs(pdnsHeader->id)};

        RMAP::iterator itFound = m_responseMap.find(k);
        RMAP::iterator itEnd = m_responseMap.end();

        if(itFound != itEnd)
        {
            if(itFound->second.state == rpHasUnfilteredData)
            {
                printf("Have the unfiltered data already");
                return true;
            }
            else
            {
                printf("NEED THE UNFILTERED DATA");
                itFound->second.state = rpNeedsUnfilteredData;
            }
        }
        return false;
    }

    unsigned char *getUnfilteredData(uint32_t requestingIP, DNSHeader *pdnsHeader, int & packetSize)
    {
        key k{requestingIP, ntohs(pdnsHeader->id)};

        RMAP::iterator itFound = m_responseMap.find(k);
        RMAP::iterator itEnd = m_responseMap.end();

        if(itFound != itEnd)
        {
            packetSize = itFound->second.size;
            return itFound->second.packet;
        }

        packetSize = 0;
        return NULL;
    }

    //Store the unfiltered response - we may need it later
    void onWaitingForFilteredData(uint32_t requestingIP, DNSHeader *pdnsHeader, unsigned char *unfilteredData, int packetSize)
    {
        printf("onWaitingForFiltered(%d, %d)\n", requestingIP, ntohs(pdnsHeader->id));
        key k{requestingIP, ntohs(pdnsHeader->id)};

        RMAP::iterator itFound = m_responseMap.find(k);
        RMAP::iterator itEnd = m_responseMap.end();

        if(itFound != itEnd)
        {
            itFound->second.state = rpHasUnfilteredData;
            itFound->second.size = packetSize;
            itFound->second.packet = new unsigned char[packetSize];
            memcpy(itFound->second.packet, unfilteredData, packetSize);
        }
    }

    bool needsUnfilteredData(uint32_t requestingIP, DNSHeader *pdnsHeader)
    {
        key k{requestingIP, ntohs(pdnsHeader->id)};

        RMAP::iterator itFound = m_responseMap.find(k);
        RMAP::iterator itEnd = m_responseMap.end();

        if(itFound != itEnd)
        {
        printf("needsUnfilteredData(%d, %d) - %s\n", requestingIP, ntohs(pdnsHeader->id), (itFound->second.state == rpNeedsUnfilteredData) ? "true" : "false");
            return(itFound->second.state == rpNeedsUnfilteredData);
        }
        printf("needsUnfilteredData(%d, %d) - false\n", requestingIP, ntohs(pdnsHeader->id));
        return false;
    }

private:
    struct key
    {
        uint32_t k1;
        uint16_t k2;

        bool operator< (key const & other) const
        {
            if(k1 == other.k1)
            {
                return k2 < other.k2;
            }
            return k1 < other.k1;
        }
    };

    struct data
    {
        unsigned filtered:1;
        unsigned unfiltered:1;

        respState state;
        uint32_t time;
        int size;
        unsigned char *packet;
        data() : filtered(0), unfiltered(0), state(rpReceived), time(::time(NULL)), size(0), packet(NULL){}
    };

    typedef std::map<key, data> RMAP;
    RMAP m_responseMap;
};



#endif //__INC_RESPONSETRACKER_H
