#include <stdio.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "LogHandler.h"
#include "utils.h"

#define TIME "w"
#define REQUESTOR_IP "i"
#define REQUESTED_HOST "h"
#define TYPE "t"
#define REASON "r"
#define CATEGORY "c"

static char b10buf[12];
static const char *base10 = "0123456789";


void LogHandler::logResponse(const char *macAddr, logEntryType type, uint32_t ipRequestor, const char *szRequestedHost, logReason reason, const char *szCategory)
{
    logprintf5("Logging response for MAC %s\n", macAddr);
    logEntry *pEntry = new logEntry();
    pEntry->openEntry();
    pEntry->addData(TYPE, type);
//    pEntry->addData(REQUESTOR_IP, ipRequestor); // not needed - log file is per device anyway
    pEntry->addData(REQUESTED_HOST, szRequestedHost);
    pEntry->addData(REASON, (uint32_t)reason);
    if(szCategory)
    {
        pEntry->addData(CATEGORY, szCategory);
    }

    addLogEntry(macAddr, pEntry);
}

void LogHandler::logCommit(int logCommitMinFreq)
{
    logMap::iterator itCur = logFiles.begin();
    logMap::iterator itEnd = logFiles.end();

    while(itCur != itEnd)
    {
        itCur->second->save(logCommitMinFreq);
        ++itCur;
    }
}

bool LogHandler::logFile::_open()
{
    if(fp)
        return true;

    //logprintf("opening file for MAC %s\n", macAddr.c_str());

    char devLogFile[256];
    //From the MAC address, work out where the config will be
    //Make sure the directory exists
    snprintf(devLogFile, sizeof(devLogFile), "%s/%s/", getenv("LOGSROOT"), macAddr.c_str());

    //logprintf("make directory %s\n", devLogFile);
    mkdir(devLogFile, 0755);

    snprintf(devLogFile, sizeof(devLogFile), "%s/%s/1.log", getenv("LOGSROOT"), macAddr.c_str());

    logprintf5("open log file %s\n", devLogFile);
    //Need to count the number of lines and update nLineCount

    fp = fopen(devLogFile, "a");
    return fp != NULL;
}

bool LogHandler::logFile::_close()
{
    if(fp)
    {
        fclose(fp);
        fp = NULL;
    }
    return true;
}

void LogHandler::logFile::_save(time_t tNow)
{
    tLastSave=tNow;
    if(queue.empty())
        return;

    if(!fp)
    {
        if(!_open()) //Failed to open logfile
            return;
    }

    LogHandler::logList::iterator itCur = queue.begin(); 
    LogHandler::logList::iterator itEnd = queue.end();

    while(itCur != itEnd)
    {
        LogHandler::logEntry *pEntry = *itCur;
        if(pEntry)
        {
            pEntry->closeEntry();
            fputs(pEntry->buf, fp);
            ++nLineCount;
            delete pEntry;
        }
        ++itCur;
    }

    queue.clear();
    fflush(fp);
}

static char *base10enc(int32_t value)
{
    bool bNeg = false;
    if(value < 0)
    {
        value = -value;
        bNeg = true;
    }
    unsigned int offset = sizeof(b10buf);

    b10buf[--offset] = '\0';
    do {
        b10buf[--offset] = base10[value % 10];
    } while (value /= 10);

    if(bNeg)
        b10buf[--offset] = '-';

    return &b10buf[offset];
}
static char *base10enc(uint32_t value)
{
    unsigned int offset = sizeof(b10buf);

    b10buf[--offset] = '\0';
    do {
        b10buf[--offset] = base10[value % 10];
    } while (value /= 10);

    return &b10buf[offset];
}

void LogHandler::logEntry::openEntry()
{
    logTime=time(NULL);
    buf[bufCnt++] = '{';
}

void LogHandler::logEntry::addData(const char *key, uint32_t value)
{
    //Check we have enough room in our buffer
    //We are adding "":, (4 chars) to each data entry + max width of unsigned b10 number (10)
    if((strlen(key) + 14) < LOG_ENTRY_SIZE - bufCnt)
    {
        if(bDataAdded)
        { //Add a preceding comma, if this is not the first item
            buf[bufCnt++] = ',';
        }
        bDataAdded = true;
        buf[bufCnt++] = '"';
        strcpy(buf+bufCnt, key);
        bufCnt += strlen(key);
        buf[bufCnt++] = '"';
        buf[bufCnt++] = ':';
        char *b10val=base10enc(value);
        strcpy(buf+bufCnt, b10val);
        bufCnt += strlen(b10val);
    }
}

void LogHandler::logEntry::addData(const char *key, int32_t value)
{
    //Check we have enough room in our buffer
    //We are adding "":, (4 chars) to each data entry + max width of signed b10 number (11 - space for the '-')
    if((strlen(key) + 15) < LOG_ENTRY_SIZE - bufCnt)
    {
        if(bDataAdded)
        { //Add a preceding comma, if this is not the first item
            buf[bufCnt++] = ',';
        }
        bDataAdded = true;
        buf[bufCnt++] = '"';
        strcpy(buf+bufCnt, key);
        bufCnt += strlen(key);
        buf[bufCnt++] = '"';
        buf[bufCnt++] = ':';
        char *b10val=base10enc(value);
        strcpy(buf+bufCnt, b10val);
        bufCnt += strlen(b10val);
    }
}
void LogHandler::logEntry::addData(const char *key, const char *value)
{
    //Check we have enough room in our buffer
    //We are adding "":"", (6 chars) to each data entry
    if((strlen(key) + strlen(value) + 6) < LOG_ENTRY_SIZE - bufCnt)
    {
        if(bDataAdded)
        { //Add a preceding comma, if this is not the first item
            buf[bufCnt++] = ',';
        }
        bDataAdded = true;
        buf[bufCnt++] = '"';
        strcpy(buf+bufCnt, key);
        bufCnt += strlen(key);
        buf[bufCnt++] = '"';
        buf[bufCnt++] = ':';
        buf[bufCnt++] = '"';
        strcpy(buf+bufCnt, value);
        bufCnt += strlen(value);
        buf[bufCnt++] = '"';
    }
}

void LogHandler::logEntry::closeEntry()
{
    const int Jan1st=1451606400;

    addData(TIME, logTime - Jan1st);
    buf[bufCnt++] = '}';
    buf[bufCnt++] = '\n';
    buf[bufCnt++] = 0;
}

