#ifndef __PROVIDERDRIVER_H__
#define __PROVIDERDRIVER_H__

#include <stdint.h>
#include <arpa/inet.h>

class providerDriverDNS
{
public:
    providerDriverDNS();
    ~providerDriverDNS(){}
    //The IP the service will respond with for a blocked request;
    bool isBlockIP(in_addr resolvedIP);
    virtual const char *getName()
    {
        return getenv("DNS_PROVIDER");
    }
private:
    in_addr blockIPs[32];
    int nBlockIPs;
};

class providerDriverCAT
{
public:
    providerDriverCAT(){}
    virtual ~providerDriverCAT(){}
    virtual bool getRemoteCategorization(const char *hostName, char *szCategory, const int nBufSize) = 0;
    static providerDriverCAT *getProvider();
    virtual const char *getName() = 0;
protected:
    virtual const char *getHTTPRequest(const char *hostName, in_addr *ip){};
    bool getCategoryFromBlockPage(const char *hostName, 
                                 const char *matchstart, 
                                 const char *matchend,
                                 char *szCategory,
                                 const int nBufSize);
    //gethostbyname wrapper
    bool getHostIP(const char * hostname , in_addr *ip);
};


class greenTeamCAT : public providerDriverCAT
{
public:
    greenTeamCAT(){}
    virtual ~greenTeamCAT(){}
    virtual bool getRemoteCategorization(const char *hostName, char *szCategory, const int nBufSize)
    {
        const char * matchstart = "color:red;\\\'>";
        const char * matchend = "</font";
        return getCategoryFromBlockPage(hostName, matchstart, matchend, szCategory, nBufSize);
    }
    virtual const char *getName()
    { return "greenteam"; }
protected:
    virtual const char *getHTTPRequest(const char *hostName, in_addr *ip)
    {
        const char request[]="GET / HTTP/1.0\r\nUser-Agent: Dougal\r\nHost: %s\r\nAccept: */*\r\n\r\n";

        snprintf(req, sizeof(req), request, hostName);
        inet_aton("81.218.119.11", ip);
        return req;
    }
private:
    char req[255];
};

class fortiGuardCAT : public providerDriverCAT
{
public:
    fortiGuardCAT(){}
    virtual ~fortiGuardCAT(){}
    virtual bool getRemoteCategorization(const char *hostName, char *szCategory, const int nBufSize)
    {
        const char * matchstart = ">Category: ";
        const char * matchend = "</h3>";
        return getCategoryFromBlockPage(hostName, matchstart, matchend, szCategory, nBufSize);
    }
    virtual const char *getName()
    { return "fortiGuard"; }
protected:
    virtual const char *getHTTPRequest(const char *hostName, in_addr *ip)
    {
        const char request[]="GET /iprep?data=%s&lookup=Lookup HTTP/1.0\r\nUser-Agent: Dougal\r\nHost: fortiguard.com\r\nAccept: */*\r\n\r\n";

        snprintf(req, sizeof(req), request, hostName);
        if(getHostIP("fortiguard.com", ip))
        {
            return req;
        }
        return NULL;
    }
private:
    char req[255];
};

#endif //__PROVIDERDRIVER_H__
