#ifndef INC_UTILS_H
#define INC_UTILS_H

#include <stdio.h>
#include "dnsrewrite.h"

void calcChecksum(const void *payload, size_t len, struct ip * ip_, struct udphdr * udp_);
int dns_strlen(unsigned char *dnsString, bool *bmCompressed = NULL);
void dns_strcpy(DNSHeader *pDNSHeader, char * dest, unsigned char *dnsString, int bufsize);
char *dns_strcpy(char *dest, const unsigned char *dnsString);
unsigned char *dns_strcpy(unsigned char *dnsString, const char *src);
bool setupDNSPacketPointers(unsigned char *pIPPacket,
                            unsigned char **ppUDPPacket,
                            unsigned char **ppDNSPacket,
                            DNSHeader **dnsHeader,
                            unsigned char **ppDNSQuestion,
                            DNSQuestion **dnsQuestion,
                            unsigned char **ppDNSAnswer,
                            DNSAAnswer **dnsAAnswer,
                            unsigned char **ppDNSFirstAnswer);

bool find_in_file(const char *file, const char *str);
bool get_state(const char *file, char * buf, uint8_t bufsize);
char * dumpBytes(const unsigned char *packet_, int length_);


//File descriptor for log file
extern FILE * logfp;

void openLogFile(const char *path, const char *mode="w");
void closeLogFile();
//O means no logging
void setLogLevel(int level);

#define logprintf(...) _logprintf(0, __VA_ARGS__)
#define logprintf1(...) _logprintf(1, __VA_ARGS__)
#define logprintf2(...) _logprintf(2, __VA_ARGS__)
#define logprintf3(...) _logprintf(3, __VA_ARGS__)
#define logprintf4(...) _logprintf(4, __VA_ARGS__)
#define logprintf5(...) _logprintf(5, __VA_ARGS__)


extern "C"
{

int _logprintf(int logLevel, const char *format, ...);

int printfile(const char *file, const char * mode, const char *format, ...);
}
#endif //INC_UTILS_H
