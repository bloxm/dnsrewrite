export ROUTERIP="192.168.0.1"
export ASSIGNEDIP="192.168.0.15"
export ASSIGNEDDNS="0.0.0.0"
export ROUTERMAC="00:00:00:00:00:00:"
export ROUTERMODEL="none"
export ROUTERPORTAL="192.168.0.36"

export NETWORK_DEFAULTS=1

#OVERRIDES for testing

#ROUTERIP_UI overrides the ROUTERIP, in the lighttp proxy config, for test purposes
#192.168.0.1 is default for the sky router
export ROUTERUI_IP="192.168.0.1"
#Override for testing
export DEFAULT_GATEWAY="192.168.0.253"
#Default for the router model
export DEFAULT_MODEL="sky_router"
export DEVBOX="192.168.0.25"

