#!/bin/bash

#called by DNSMasq
#see etc.dnsmasq.conf
#--dhcp-script=<path>

#if running as root - exec as user dnsrewrite
#Dont log before ownership chance since log file will have root ownership.
#echo $@ >> /tmp/dhcpenv
#env >> /tmp/dhcpenv

#exec >> /tmp/dhcpenv
#exec 2>&1

#set -x

. /dnsrewrite.inc


if [[ "${UID}" != "${DNSREWRITE_UID}" ]] ; then
    CMD="${0} $(printf "'%s' " "${@}")"
    exec su -s /bin/bash -c "$CMD" dnsrewrite
fi

echo $@ >> /tmp/dhcpenv
env >> /tmp/dhcpenv

exec >> /tmp/dhcpenv
exec 2>&1

set -x


COMMAND=$1
MACADDR=$2
IPADDR=$3

DNSTMPDIR="${DHCPDIR}/${MACADDR}"
DEVICEDIR="${DEVICESROOT}/${MACADDR}"

#Not interested in devices on wlan0 
if [[ "${DNSMASQ_INTERFACE}" == "wlan0" ]] ; then
    exit 0
fi

#When a device gets an IP from us, link it to its config

if [[ $1 == 'add' || $1 == 'old' ]] ; then
    mkdir -p "${DNSTMPDIR}"
    #Need to prevent ln from following the link that is there already! Lets remove it first
    rm -f "${DHCPDIR}/${IPADDR}"
    ln -sf "./${MACADDR}" "${DHCPDIR}/${IPADDR}"

    if [[ -d ${DEVICEDIR} ]] ; then
        #This device has already got a policy/config
        #Need to prevent ln from following the link that is there already! Lets remove it first
        rm -f "${DNSTMPDIR}/config"

        ln -sf ${DEVICEDIR} "${DNSTMPDIR}/config"
        ln -sf "${DEVICEDIR}/state" "${DNSTMPDIR}/state"
        ${SCRIPTDIR}/generateHostInfo.sh
    else
	    echo -n "${DNSMASQ_SUPPLIED_HOSTNAME}" > ${DNSTMPDIR}/name
	    echo -n "UNREGISTERED" > ${DNSTMPDIR}/state
    fi
fi


