#!/bin/bash
#on the client - ./scripts/remote-gdb 192.168.1.1:2345 ./build_dir/target-mipsel_24kec+dsp_uClibc-0.9.33.2/dnsrewrite/dnsrewrite
# gdb-multiarch -x ~/remotegdb ./build_dir/target-mipsel_24kec+dsp_uClibc-0.9.33.2/dnsrewrite/dnsrewrite
# ~/remotegdb should contain
# set sysroot /home/nick/bloxm/protexm.working/openwrt/scripts/../staging_dir/target-mipsel_24kec+dsp_uClibc-0.9.33.2/root-ramips/
# target extended-remote 192.168.0.224:2345
# set history filename /home/nick/bloxm/protexm.working/openwrt/scripts/../tmp/.gdb_history
# set history size 100000000
# set history save on
#

. /dnsrewrite.inc

exec gdbserver 192.168.1.15:2345 dnsrewrite

