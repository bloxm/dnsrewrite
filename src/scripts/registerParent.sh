#!/bin/bash

#called by parentreg.cgi

. /dnsrewrite.inc

#if running as root - exec as user dnsrewrite or fail
if [[ "${UID}"  != "${DNSREWRITE_UID}" ]] ; then
    CMD="${0} $(printf "'%s' " "${@}")"
    exec su -s /bin/bash -c "$CMD" dnsrewrite
fi

echo $@ >> /tmp/regparent

exec >> /tmp/regparent

set -x

EMAILADDR="${1}"
PASSWORD="${2}"
IPADDR="${3}"
DEVNAME="${4}"

#Validate parameters
DEFPOLICYABSDIR="${DNSREWRITEDIR}/default-policy/"

[[ "$#" == 4 ]] || die "Incorrect no of arguments, provided $#"

echo ${IPADDR} | grep -E -q '^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$' || die "${IPADDR} does not look like an IP address"
echo ${DEVNAME} | grep -E -q '([.]{2}|[;$/\"])' && die "${DEVNAME} has invalid characters"


if [[ ! -d "${DHCPDIR}/${IPADDR}" ]] ; then
    #We have a parent request from a device that has not DHCP from us - what shall we do
    die "Device on IP ${IPADDR} did not DHCP from us"
fi

#Need to work out the MACADDR of the device, to perform the registration
MACADDR="$(basename $(readlink -f ${DHCPDIR}/${IPADDR}))"


#if [[ ! -f "${STATEFILE}" || $(cat "${STATEFILE}") != ${SYSSTATE_CONFIGURED} ]] ; then
    echo -n "${SYSSTATE_CONFIGURED}" > "${STATEFILE}"
#    echo -n "${EMAILADDR}" > "${CONFIGDIR}/parent_email"
#    echo -n "${PASSWORD}" > "${CONFIGDIR}/parent_password"

#    mkdir -p ${DEVICEDIR}
#    echo -n ${STATE_UNFILTERED} > ${DEVICEDIR}/state

    #register the parent device as unfiltered
    ${SCRIPTDIR}/setDeviceState.sh ${MACADDR} "${STATE_REGISTERED}" "${DEVNAME}"
    ${SCRIPTDIR}/setDeviceState.sh ${MACADDR} "${STATE_UNFILTERED}"
#else
#    die "Already configured"
#fi

