#!/bin/bash

#See also etc.init.d.systemstate

exec 2>> /tmp/onSystemConfigUpdate 
date >&2
echo $* >&2

. /dnsrewrite.inc

LASTSTATEFILE="${RUNDIR}/.last_state"
LASTDNSPROVFILE="${RUNDIR}/.last_dnsprov"
LASTCATPROVFILE="${RUNDIR}/.last_catprov"
LASTLOCALBLOCKFILE="${RUNDIR}/.last_localblock"
LASTSUBSTATEFILE="${RUNDIR}/.last_substate"
LASTLANINFO="${RUNDIR}/.last_laninfo"

SERVICESFILE="${CONFIGDIR}/services"

TIMESTAMPFILES=("${CONFIGDIR}.tstamp:wc" "${LANINFO}.tstamp:wc" "${DEVICESROOT}.tstamp:wc" "${PASSWDDIR}.tstamp:wc")


sumDir()
{
    SUM=$(echo -n $(md5sum ${1}/* | md5sum - | awk '{print $1}'))
    echo -n "$SUM"
}


if [[ $1 == "init" ]] ; then
    for TSFILE in "${TIMESTAMPFILES[@]%:wc}" ; do
        [ -f "${TSFILE}" ] || {
            touch "${TSFILE}"
            chown "${DNSREWRITE_UID}:${DNSREWRITE_GID}" "${TSFILE}"
        }
    done

    echo -n "${SYSSTATE}" > "${LASTSTATEFILE}"
    echo -n "${DNS_PROVIDER}" > "${LASTDNSPROVFILE}"
    echo -n "${CAT_PROVIDER}" > "${LASTCATPROVFILE}"
    echo -n "${LOCALBLOCK}" > "${LASTLOCALBLOCKFILE}"
    echo -n "${SUBSTATE}" > "${LASTSUBSTATEFILE}"
    echo -n "RUNNING" > "${SERVICESFILE}"
    ckSum=$(sumDir "${LANINFO}")
    echo -n ${ckSum} > "${LASTLANINFO}"
    echo "${TIMESTAMPFILES[@]}"
    exit 0
fi

if [[ $1 == "stop" ]] ; then
    echo -n "STOPPING" > ${SERVICESFILE}
    /etc/init.d/tcpproxy stop 
    /etc/init.d/goahead stop
    /etc/init.d/dnsrewrite stop
    /etc/init.d/dnsmirror stop
    /etc/init.d/dnsmasq-filtered stop
    /etc/init.d/dnsmasq-unfiltered stop
    /etc/init.d/dnsmasq-local stop
    /etc/init.d/dnsmasq-pairing stop
    echo -n "STOPPED" > ${SERVICESFILE}
    exit 0
fi

if [[ $1 == "start" ]] ; then
    echo -n "STARTING" > ${SERVICESFILE}
    /etc/init.d/tcpproxy start 
    /etc/init.d/goahead start
    /etc/init.d/dnsrewrite start
    /etc/init.d/dnsmirror start
    /etc/init.d/dnsmasq-filtered start
    /etc/init.d/dnsmasq-unfiltered start
    /etc/init.d/dnsmasq-local start
    /etc/init.d/dnsmasq-pairing start
    echo -n "RUNNING" > ${SERVICESFILE}
    exit 0
fi

if [[ $1 == "restart" ]] ; then
    $0 stop
    $0 start
    exit 0
fi

set -x

#This script called by BusyBox inotifyd when the config timestamp is changed (by the UI / fsjson)
#PROG ACTUAL_EVENTS FILEn [SUBFILE] is run

#We should be registered for only metadata events
if [[ ( "$1" != "w" && "$1" != "c" ) ]] ; then
    exit 0
fi

#config/laninfo/devices

TRIGGER=$(basename $2 .tstamp)


case "${TRIGGER}" in

    "config")
        if [[ "${SYSSTATE}" != "$(cat ${LASTSTATEFILE})" ]] ; then
            #Store the last state
            echo -n "${SYSSTATE}" > "${LASTSTATEFILE}"
            RESTART_NETWORK=1
            #Because the auth details may have changed
            RESTART_GOAHEAD=1
        fi

        if [[ "${DNS_PROVIDER}" != "$(cat ${LASTDNSPROVFILE})" ]] ; then
            #Store the last dns provider
            echo -n "${DNS_PROVIDER}" > "${LASTDNSPROVFILE}"
            RESTART_DNSMASQ_FILTERED=1
            RESTART_DNSREWRITE=1
        fi

        if [[ "${CAT_PROVIDER}" != "$(cat ${LASTCATPROVFILE})" ]] ; then
            #Store the last dns provider
            echo -n "${CAT_PROVIDER}" > "${LASTCATPROVFILE}"
            RESTART_DNSREWRITE=1
        fi

        if [[ "${LOCALBLOCK}" != "$(cat ${LASTLOCALBLOCKFILE})" ]] ; then
            #Store the last dns provider
            echo -n "${LOCALBLOCK}" > "${LASTLOCALBLOCKFILE}"
            RESTART_DNSREWRITE=1
        fi

        if [[ "${SUBSTATE}" != "$(cat ${LASTSUBSTATEFILE})" ]] ; then
            #Store the last dns provider
            echo -n "${SUBSTATE}" > "${LASTSUBSTATEFILE}"
            RESTART_DNSMASQ_PAIRING=1
        fi

        ;;

    "passwd")
            #Because the auth details have changed
            RESTART_GOAHEAD=1
        ;;


    "laninfo")
        ckSum=$(sumDir "${LANINFO}")

        if [[ "${ckSum}" != "$(cat ${LASTLANINFO})" ]] ; then
            RESTART_NETWORK=1
            RESTART_DNSMIRROR=1
            RESTART_DNSMASQ_LOCAL=1
        fi
        ;;


    "devices")
        RESTART_GENHOSTINFO=1
        ;;
esac

echo -n "RESTARTING" > ${SERVICESFILE}

[[ $RESTART_NETWORK ]] && ${SCRIPTDIR}/configureNetwork.sh
[[ $RESTART_GENHOSTINFO ]] && ${SCRIPTDIR}/generateHostInfo.sh
[[ $RESTART_NETWORK ]] && /etc/init.d/tcpproxy restart 
[[ $RESTART_GOAHEAD ]] && /etc/init.d/goahead restart
[[ $RESTART_DNSREWRITE ]] && /etc/init.d/dnsrewrite restart
[[ $RESTART_DNSMIRROR ]] && /etc/init.d/dnsmirror restart
[[ $RESTART_DNSMASQ_FILTERED ]] && /etc/init.d/dnsmasq-filtered restart
[[ $RESTART_DNSMASQ_UNFILTERED ]] && /etc/init.d/dnsmasq-unfiltered restart
[[ $RESTART_DNSMASQ_LOCAL ]] && /etc/init.d/dnsmasq-local restart
[[ $RESTART_DNSMASQ_PAIRING ]] && /etc/init.d/dnsmasq-pairing restart

sleep 3

echo -n "RUNNING" > ${SERVICESFILE}


