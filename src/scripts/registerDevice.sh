#!/bin/bash

#called by devicereg.cgi

. /dnsrewrite.inc

#if running as root - exec as user dnsrewrite or fail
if [[ "${UID}"  != "${DNSREWRITE_UID}" ]] ; then
    CMD="${0} $(printf "'%s' " "${@}")"
    exec su -s /bin/bash -c "$CMD" dnsrewrite
fi

echo $@ >> /tmp/regdevice

exec >> /tmp/regdevice

#set -x

NAME="${1}"
IPADDR="${2}"

#Validate parameters
[[ "$#" == 2 ]] || die "Incorrect no of arguments, provided $#"

echo ${IPADDR} | grep -E -q '^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$' || die "${IPADDR} does not look like an IP address"

echo ${NAME} | grep -E -q '([.]{2}|[;$/\"])' && die "${NAME} has invalid characters"


if [[ ! -d "${DHCPDIR}/${IPADDR}" ]] ; then
    #We have a parent request from a device that has not DHCP from us - what shall we do
    die "Device on IP ${IPADDR} did not DHCP from us"
fi

#Need to work out the MACADDR of the device, to perform the registration
MACADDR="$(basename $(readlink -f ${DHCPDIR}/${IPADDR}))"

#register the device
${SCRIPTDIR}/setDeviceState.sh ${MACADDR} "${STATE_REGISTERED}" "${NAME}"
