#!/bin/bash

. /dnsrewrite.inc

#This script waits for the ethernet link, on the specified interface
#It flashes the LEDS until the link is up

#not currently used

ETH=$1

LINK="/sys/class/net/$ETH/carrier"

${SCRIPTDIR}/gpio.sh SETUPLEDS

LED=("GREEN" "BLUE" "GREEN" "BLUE")
STATE=("1" "1" "0" "0")

index=0

while [[ $(cat $LINK) == "0" ]]
do
    ${SCRIPTDIR}/gpio.sh ${LED[$index]} ${STATE[$index]}

    index=$((index+1))

    if [[ $index == 4 ]] ; then
        index=0
    fi
done
