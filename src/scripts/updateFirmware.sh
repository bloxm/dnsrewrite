#!/bin/bash

if [[ $1 == "PRISTINE" ]] ; then
    >/etc/sysupgrade.conf
else
cat <<HERE > /etc/sysupgrade.conf
/etc/dnsrewrite/devices
/etc/dnsrewrite/dhcp
/etc/dnsrewrite/laninfo
/etc/dnsrewrite/passwd
/etc/dnsrewrite/config
HERE
fi

if [[ $1 == "RAMFS" ]] ; then
    sysupgrade http://192.168.0.25/firmware/openwrt-ramips-mt7620-wt3020-8M-initramfs-uImage.bin
else
    sysupgrade http://192.168.0.25/firmware/openwrt-ramips-mt7620-wt3020-8M-squashfs-sysupgrade.bin
fi

