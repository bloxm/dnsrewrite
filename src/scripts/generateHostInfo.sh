#!/bin/bash

. /dnsrewrite.inc


#Take the information for the dnsrewrite configuration, and build a /var/dnsmasq/hosts and /var/dnsmasq/dhcp

mkdir -p /var/run/dnsmasq

HOSTSFILE=/var/run/dnsmasq/hosts
DHCPFILE=/var/run/dnsmasq/dhcp
TMPHOSTSFILE=$(mktemp /var/run/dnsmasq/hostsXXXXXXXX)
TMPDHCPFILE=$(mktemp /var/run/dnsmasq/dhcpXXXXXXXX)


#For each device we have a triplet of IP MAC NAME

for f in ${DHCPDIR}/* ; do
    if [[ -L $f ]] ; then
        if [[ -s $f/config/name ]] ; then
            IP=$(basename $f)
            MAC=$(basename $(readlink -f $f))
            NAME=$(cat $f/config/name)


            echo "$IP $NAME" >> $TMPHOSTSFILE
            echo "$MAC,$NAME" >> $TMPDHCPFILE
        fi
    fi
done

mv $TMPHOSTSFILE $HOSTSFILE
mv $TMPDHCPFILE $DHCPFILE

chmod 644 $HOSTSFILE
chmod 644 $DHCPFILE

chown ${DNSREWRITE_UID}:${DNSREWRITE_GID} ${HOSTSFILE}
chown ${DNSREWRITE_UID}:${DNSREWRITE_GID} ${DHCPFILE}


if [[ "$1" != "boot" ]] ; then
    exec killall -HUP dnsmasq
fi
