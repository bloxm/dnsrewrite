#!/bin/bash

exec >> /tmp/onDHCPFromRouter
exec 2>&1
date

echo "Params: $@"
. /dnsrewrite.inc

export router
export subnet
export domain
export interface
export dns
export ip
export serverid

#There is a special case that needs handling - if we are pairing with a router, it may take the network down
#we will come up again, but now the router wont be serving DHCP, we need to change our interface to a static interface on the same IPs
#
echo "Info: $1 - ${SYSSTATE} - ${SUBSTATE} - ${NET_ASSIGNEDIP}"

if [[ "${SYSSTATE}" == "${SYSSTATE_PRISTINE}" && "${SUBSTATE}" == "PAIRING"  && "$1" == "deconfig" && -n "${NET_ASSIGNEDIP}" ]] ; then
    echo "Static assigning of IP - router DHCP will have gone away"
    ifconfig eth0 ${NET_ASSIGNEDIP} up
    /etc/init.d/tcproxy restart
fi



#expecting bound or renew in $1
if [[ $1 == "bound" || $1 == "renew" ]] ; then
    ${SCRIPTDIR}/lanInfo.sh $1
    exec ${SCRIPTDIR}/configureNetwork.sh dhcp
fi
