#!/bin/bash

. /dnsrewrite.inc

#Need to ensure the network config is the appropriate state

exec >>/tmp/configNet
exec 2>&1
set -x

#Make sure we have a sensible network configuration
rm -f /tmp/resolv.conf
echo "nameserver 8.8.8.8" > /tmp/resolv.conf

NETFILE=""
WIFIFILE=""

#If we are booting up, in pristine state, we should ignore all the laninfo (i.e. NET_DHCPFAIL)
if [ "${SYSSTATE}" == "${SYSSTATE_PRISTINE}" ] ; then
    if [[ "${NET_DHCPFAIL}" == "1" ]] ; then
        LINK="/sys/class/net/eth0/carrier"
        if [[ $(cat $LINK) == "0" ]] ; then
            export APPLIANCESSID="BLOXM (LAN disconnected)"
        else
            export APPLIANCESSID="BLOXM :( ${NET_APPLIANCEIP}"
        fi

        WIFIFILE="${SYSCFGDIR}/etc.config.wireless"
        NETFILE="${SYSCFGDIR}/etc.config.network-pristine-static"
    else
        export APPLIANCESSID="BLOXM :) http://${NET_ASSIGNEDIP}/"
        WIFIFILE="${SYSCFGDIR}/etc.config.wireless"
        NETFILE="${SYSCFGDIR}/etc.config.network-pristine-dhcp"
    fi
else
    #We need to used our applianceip and appliance network in the config

    #Should really be disabling wireless - or making it a client
    export APPLIANCESSID="BLOXM :() http://${NET_APPLIANCEIP}"
    WIFIFILE="${SYSCFGDIR}/etc.config.wireless"
    NETFILE="${SYSCFGDIR}/etc.config.network-withaliases"
fi

if [[ "$1" == "boot" ]] ; then
    rm -f /etc/config/wireless
    rm -f /etc/config/network
    $patchConfig  "${WIFIFILE}" > /etc/config/wireless
    $patchConfig "${NETFILE}"  > /etc/config/network
else
    $patchConfig  "${WIFIFILE}" | uci import wireless 
    $patchConfig "${NETFILE}" | uci import network
fi

#On a running system (not during bootup) we need to reconfigure the network
#We use OpenWrt config/procd system to trigger restarting of all of our services (if necessary)
#Order is important here, and we need the network to be reconfigured, before we start the services

if [[ "$1" != "boot" ]] ; then
    /etc/init.d/network reload
    sleep 2
    reload_config

        #Need to clear out any temporary network
        #May have been created during pairing
        #Anyway this makes sure eth0 cant interfere with anything
#        ifconfig eth0 0.0.0.0

    #Update the SSID
    wifi reload

    ##Make sure tcpproxy is cool
    /etc/init.d/tcpproxy restart
    ##Make sure our emergency interface is up
    /etc/init.d/emergency restart
fi

#Really Make sure we have a sensible network configuration
echo "nameserver 8.8.8.8" > /tmp/resolv.conf

