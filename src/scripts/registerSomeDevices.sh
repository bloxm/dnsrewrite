#!/bin/bash


echo -n BASICFILTER > /etc/dnsrewrite/config/state
rm -rf /tmp/dnsrewrite/dhcp/*
rm -rf /etc/dnsrewrite/devices/*
/etc/init.d/bloxm restart

./OnDHCP.sh add b0:0b:1e:50:00:01 192.168.1.11
./OnDHCP.sh add b0:0b:1e:50:00:02 192.168.1.12
./OnDHCP.sh add b0:0b:1e:50:00:03 192.168.1.13
./OnDHCP.sh add b0:0b:1e:50:00:04 192.168.1.14
./OnDHCP.sh add b0:0b:1e:50:00:05 192.168.1.15
./OnDHCP.sh add b0:0b:1e:50:00:06 192.168.1.16

#These are DHCP only
./OnDHCP.sh add b0:0b:1e:50:00:07 192.168.1.17
./OnDHCP.sh add b0:0b:1e:50:00:08 192.168.1.18
./OnDHCP.sh add b0:0b:1e:50:00:09 192.168.1.19
./OnDHCP.sh add b0:0b:1e:50:00:10 192.168.1.20

./registerParent.sh "parent@parent" "password" 192.168.1.11 "DEV-1"
./registerDevice.sh 'Nicks phone' 192.168.1.12
./registerDevice.sh 'Max Pad' 192.168.1.13
./registerDevice.sh 'Dads PC' 192.168.1.14
./registerDevice.sh 'Dads Phone' 192.168.1.15
./registerDevice.sh 'Mums iPad' 192.168.1.16

./setDeviceState.sh b0:0b:1e:50:00:02 FILTERED High
./setDeviceState.sh b0:0b:1e:50:00:03 FILTERED Medium
./setDeviceState.sh b0:0b:1e:50:00:06 FILTERED Monitor
