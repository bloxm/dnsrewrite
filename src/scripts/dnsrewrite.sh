#!/bin/bash

. /dnsrewrite.inc

#Ensure correct ownership
chgrp "${DNSREWRITE_GID}" "${DNSREWRITEDIR}"
chmod g+w "${DNSREWRITEDIR}"

mkdir -p "${CONFIGDIR}"
chgrp "${DNSREWRITE_GID}" "${CONFIGDIR}"
chmod g+w "${CONFIGDIR}"

mkdir -p "${DEVICESROOT}"
chgrp "${DNSREWRITE_GID}" "${DEVICESROOT}"
chmod g+w "${DEVICESROOT}"

kill -9 $(cat /var/run/dnsrewrite.pid)
exec dnsrewrite "/var/run/dnsrewrite.pid"
