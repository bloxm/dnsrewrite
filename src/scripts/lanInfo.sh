#!/bin/bash

. /dnsrewrite.inc

exec >>/tmp/lanInfo
exec 2>&1
echo "START"

set -x
date

#This script configures the contents of laninfo.
#In PRISTINE mode, it runs at boot and when DHCP activity occurs (we are assigned an IP)
#Its role is to determine if we are connected to a router, and check whether DHCP is being served
#If there is no DHCP it loads default configuration, which includes the definition of NETWORK_DEFAULTS
#which infers the defaulting of various settings
#Otherwise examine the current network settings (as assigned by DHCP), and update our laninfo DB

LINFODIR="${DNSREWRITEDIR}/laninfo/"

mkdir -p ${LINFODIR}

#Clear out the current laninfo
rm -f ${LINFODIR}/*


if [ "${SYSSTATE}" != "${SYSSTATE_PRISTINE}" ] ; then
    echo "LANINFO boot, can only be run in PRISTINE mode"
    exit 0
fi

#First things first - we rely on DHCP environment variables later in the this script, however we may already have some in our environment
#lets clear them out

DHCPVARS="router subnet domain interface dns ip serverid"

for i in ${DHCPVARS} ; do
    unset $i
done 



#First lets see if we can get an IP on eth0, via DHCP
SCRIPT=$(mktemp)
PRIMARYSCRIPTOUT=$(mktemp)
TEMPSCRIPTOUT=$(mktemp)
echo "#!/bin/bash" >> $SCRIPT
echo "env" >> $SCRIPT
chmod +x $SCRIPT

ORIGIP=$(ifconfig eth0 | grep "inet addr" | cut -d : -f 2 | cut -d ' ' -f 1)

ifconfig eth0
ifconfig eth0 up
ifconfig eth0
udhcpc -n -f -q -s $SCRIPT -i eth0 | grep -e "router=" -e "subnet=" -e "domain=" -e "interface=" -e "dns=" -e "ip=" -e "serverid=" | sed -e 's/=/="/g' -e 's/$/"/g' -e 's/^/primary_/g'
udhcpc -n -f -q -s $SCRIPT -i eth0 | grep -e "router=" -e "subnet=" -e "domain=" -e "interface=" -e "dns=" -e "ip=" -e "serverid=" | sed -e 's/=/="/g' -e 's/$/"/g'  -e 's/^/primary_/g' > $PRIMARYSCRIPTOUT

. $PRIMARYSCRIPTOUT
rm -f $PRIMARYSCRIPTOUT

#ifconfig eth0 down

if [[ -n $primary_ip ]] ; then
    #We need to grab a second IP address from DHCP for the router control
    #Lets get it now so we dont risk messing up OpenWRT later

    ifconfig eth0 down

    ORIGMAC=$(cat /sys/class/net/eth0/address)
    TEMPMAC="0A:DE:AD:BE:EF:0A"
    ifconfig eth0 hw ether "${TEMPMAC}"
    ifconfig eth0 up
    ifconfig eth0
    udhcpc -n -f -q -s $SCRIPT -i eth0 | grep -e "router=" -e "subnet=" -e "domain=" -e "interface=" -e "dns=" -e "ip=" -e "serverid=" | sed -e 's/=/="/g' -e 's/$/"/g' -e 's/^/spare_/g'
    #rename environment variables so we dont pollute the environment
    udhcpc -n -f -q -s $SCRIPT -i eth0 | grep -e "router=" -e "subnet=" -e "domain=" -e "interface=" -e "dns=" -e "ip=" -e "serverid=" | sed -e 's/=/="/g' -e 's/$/"/g'  -e 's/^/spare_/g' > $TEMPSCRIPTOUT

    cat $TEMPSCRIPTOUT
    . $TEMPSCRIPTOUT
    rm -f $TEMPSCRIPTOUT

    ifconfig eth0 down
    ifconfig eth0 hw ether "${ORIGMAC}"
    ifconfig eth0 up
    ifconfig eth0
fi

rm -f $SCRIPT

if [[ -n $primary_ip ]] ; then

    if [[ -z $spare_ip || $spare_ip == $primary_ip ]] ; then
        #Need to find a suitable address for the router portal
        :
    fi
    #We were assigned an IP - we can do nothing, when the network comes up later, we will be called again - via the dhcp script
    echo "We are being served DHCP - $primary_ip"
    #Allow this script to continue on the basis of the DHCP info we got
else
    #No DHCP server, maybe we are not plugged in!
    #Load the network defaults - which include a dhcp client
    echo "Loading network defaults"
    . "${SCRIPTDIR}/def-network.inc"

    #continue
fi


#route -n
#ifconfig eth0
#cat /etc/resolv.conf

if [[ -z "${NETWORK_DEFAULTS}" ]] ; then
    #get this from the DHCP environment - its easier to parse if other interfaces are present
    GATEWAY=$primary_router
    ROUTERIP=$primary_serverid
    ASSIGNEDIP=$primary_ip
    ASSIGNEDDNS=$primary_dns
    ROUTERPORTAL=$spare_ip
    
#    ROUTERIP=$(route -n | head -3 | tail -1 | awk '{print $2}')
#    ASSIGNEDIP=$(ifconfig eth0 | grep inet | sed -e "s/.*addr://g" -e "s/\s*Bcast.*//g" |  awk '{print $1}')
#    ASSIGNEDDNS=$(grep nameserver /etc/resolv.conf | awk '{print $2}' | uniq | awk '{print $1}')

    #Get the router into the arp table
    ping -c 2 "${ROUTERIP}"
    ROUTERMAC=$(arp -n | grep -e "${ROUTERIP}\W" | awk '{print $4}')
#    ROUTERMODEL=$(curl -I "${ROUTERIP}" 2>/dev/null| grep Server | tr -d '\r' | awk '{printf $2}')
fi

#Work out a good address for us we need to be on the same subnet as the router, but not clash with it.
#Assume for now all IPs are DHCP'd.

OLDIFS=$IFS
IFS=. 
read ip1 ip2 ip3 ip4 <<< "${ROUTERIP}"
IFS=$OLDIFS

APPLIANCESUBNET="${ip1}.${ip2}.${ip3}"

if [[ ${ip4} -lt 128 ]] ; then
    APPLIANCEIP="${APPLIANCESUBNET}.224"
else
    APPLIANCEIP="${APPLIANCESUBNET}.1"
fi

#APPLIANCENETPRE is prefixed to IP addresses, without a '.'
#It is basically used to determine where in the network range we put the IPs that 
#we use for services we run on the LAN
#So (assuming 192.168.1.1 - to start at 192.168.1.52 APPLIANCENETPRE should be equal to 192.168.1.5
#So (assuming 192.168.1.1 - to start at 192.168.1.2 APPLIANCENETPRE should be equal to 192.168.1.
#APPLIANCENETPRE must only be used to define addresses above .1 and below .254 (

#We have 2 addresses that we know about
#Our IP, and the 


APPLIANCENETPREFIX="${ip1}.${ip2}.${ip3}.4"

if [[ -z "${ROUTERPORTAL}" ]] ; then
    ROUTERPORTAL="${APPLIANCENETPREFIX}6"
fi

echo -n "${ROUTERIP}" > "${LINFODIR}/routerip"
echo -n "${GATEWAY}" > "${LINFODIR}/gateway"
echo -n "${ROUTERIP}" > "${LINFODIR}/routerui_ip"
echo -n "${ASSIGNEDIP}" > "${LINFODIR}/assignedip"
echo -n "${ASSIGNEDDNS}" > "${LINFODIR}/assigneddns"
echo -n "${ROUTERMAC}" > "${LINFODIR}/routermac"
#echo -n "${ROUTERMODEL}" > "${LINFODIR}/routermodel"
echo -n "${ROUTERPORTAL}" > "${LINFODIR}/routerportal"
echo -n "${APPLIANCEIP}" > "${LINFODIR}/applianceip"
echo -n "${APPLIANCESUBNET}" > "${LINFODIR}/appliancesubnet"
echo -n "${APPLIANCENETPREFIX}" > "${LINFODIR}/appliancenetprefix"

if [[ -n "${DEVBOX}" ]] ; then
    echo -n "${DEVBOX}" > "${LINFODIR}/devbox"
fi


if [[ -n "${NETWORK_DEFAULTS}" ]] ; then
    echo -n "1" > "${LINFODIR}/dhcpfail"
    if [[ -n "${ROUTERUI_IP}" ]] ; then
        echo -n "${ROUTERUI_IP}" > "${LINFODIR}/routerui_ip"
    fi

    if [[ -n "${DEFAULT_GATEWAY}" ]] ; then
        echo -n "${DEFAULT_GATEWAY}" > "${LINFODIR}/gateway"
    fi

    if [[ -n "${DEFAULT_MODEL}" ]] ; then
        echo -n "${DEFAULT_MODEL}" > "${LINFODIR}/routermodel"
    fi
else
    echo -n "0" > "${LINFODIR}/dhcpfail"
fi

chown -R ${DNSREWRITE_UID}:${DNSREWRITE_GID} ${LINFODIR}

#debug
#. /dnsrewrite.inc
#env
#end debug

exit 0
