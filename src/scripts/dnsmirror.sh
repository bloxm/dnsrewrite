#!/bin/bash

. /dnsrewrite.inc


kill -9 $(cat /var/run/dnsmirror.pid)

exec dnsmirror "/var/run/dnsmirror.pid"
