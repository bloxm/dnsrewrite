#!/bin/bash

. /dnsrewrite.inc

#kill any previous filtered dnsmasq instance

kill $(cat /var/run/dnsmasq/dnsmasq.filtered.pid)
kill $(cat /var/run/dnsmasq/dnsmasq.unfiltered.pid)

#start a new filtered dnsmasq instance
/usr/sbin/dnsmasq -C /etc/dnsmasq.filtered.conf -x /var/run/dnsmasq/dnsmasq.filtered.pid
/usr/sbin/dnsmasq -C /etc/dnsmasq.unfiltered.conf -x /var/run/dnsmasq/dnsmasq.unfiltered.pid

cat /var/run/dnsmasq/dnsmasq.*.pid

