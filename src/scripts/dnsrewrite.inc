#!/bin/sh

#DO NOT USE BASH EXTENSIONS IN HERE


export DNS_GLOBAL="8.8.8.8"
export DNS_UPSTREAMFILTER="81.218.119.11"

export DNS_UNFILTERED_IP="192.168.10.2"
export DNS_FILTERED_IP="192.168.10.3"
export TAP_IP_ADDRESS="192.168.10.1"
export TAP_MAC_ADDRESS="0A:1C:C2:19:7A:01"

export WIFI_PORTAL="10.10.2.1"
export TCPPROXY_PORT="2324"

export DNSREWRITE_UID="501"
export DNSREWRITE_GID="501"

export RUNDIR="/var/dnsrewrite"
export DNSREWRITEDIR="${ROOT_PREFIX}/etc/dnsrewrite"

export DNSREWRITEBINDIR="${ROOT_PREFIX}/bin/bloxm"

#Needs to persist
export DHCPDIR="${DNSREWRITEDIR}/dhcp"
export SESSIONDIR="${RUNDIR}/session"
export SESSIONFILE="${SESSIONDIR}/.key"
export SCRIPTDIR="${DNSREWRITEBINDIR}"
export IPTABLESDIR="${DNSREWRITEBINDIR}/iptables"
export DEVICESROOT="${DNSREWRITEDIR}/devices"
export LOGSROOT="${DNSREWRITEDIR}/logs"
export LANINFO="${DNSREWRITEDIR}/laninfo"
export SYSCFGDIR="${DNSREWRITEBINDIR}/syscfg"
export POLICYDIR="${DNSREWRITEDIR}/policies"
export DNSPROVDIR="${DNSREWRITEDIR}/dnsproviders"
export CATPROVDIR="${DNSREWRITEDIR}/catproviders"
export DIR="${DNSREWRITEDIR}/policies"
export DEFPOLICYDIR="${DNSREWRITEDIR}/default-policy"
export ROUTERPLUGINS="/www/adminportal/js/routercontrol/plugins"

export PASSWDDIR="${DNSREWRITEDIR}/passwd"
export CONFIGDIR="${DNSREWRITEDIR}/config"
export STATEFILE="${CONFIGDIR}/state"
export DNSPROVFILE="${CONFIGDIR}/dnsprovider"
export CATPROVFILE="${CONFIGDIR}/catprovider"
export LOCALBLOCKFILE="${CONFIGDIR}/localblock"
export SUBSTATEFILE="${CONFIGDIR}/substate"
export PASSWDFILE="${PASSWDDIR}/.passwd"
export ADMINUSERNAMEFILE="${CONFIGDIR}/parent_email"
export SYSSTATE=$(cat ${STATEFILE} 2>/dev/null )
export SUBSTATE=$(cat ${SUBSTATEFILE} 2>/dev/null )
export DNS_PROVIDER=$(cat ${DNSPROVFILE} 2>/dev/null )
export CAT_PROVIDER=$(cat ${CATPROVFILE} 2>/dev/null )
export LOCALBLOCK=$(cat ${LOCALBLOCKFILE} 2>/dev/null )

#limit to 20 characters 
export SYSSTATE_PRISTINE="PRISTINE" 
export SYSSTATE_BASICFILTER="BASICFILTER"
export SYSSTATE_UNCONFIGURED="UNCONFIGURED" #Taken to parent reg - not used?
export SYSSTATE_CONFIGURED="CONFIGURED"
export SYSSTATE_SUBSCRIBED="SUBSCRIBED"

#Device states
export STATE_UNREGISTERED="UNREGISTERED"   #Equivalent to BASICFILTER
export STATE_REQUESTNAME="REQUESTNAME"     #Taken to holding pen
export STATE_NEEDSAPPROVAL="NEEDSAPPROVAL" #Taken to holding pen
export STATE_UNFILTERED="UNFILTERED"	  
export STATE_FILTERED="FILTERED"
export STATE_MONITORED="MONITORED"
export STATE_BLOCKED="BLOCKED"

patchConfig="${SCRIPTDIR}/patchConfig.sh"

if [[ -d ${LANINFO}  && "$(ls -A ${LANINFO})" ]] ; then
    for f in ${LANINFO}/* ; do eval "export NET_$(basename $f | tr '[a-z]' '[A-Z]')=\"$(cat $f)\"" ; done
fi

export ADMIN_PORTAL="${NET_APPLIANCENETPREFIX}2"
export REG_PORTAL="${NET_APPLIANCENETPREFIX}3"
export BLOCK_PORTAL="${NET_APPLIANCENETPREFIX}4"
export HOLD_PORTAL="${NET_APPLIANCENETPREFIX}5"


die()
{
    echo $1
    exit 1
}


