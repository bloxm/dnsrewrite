#!/bin/bash

. /dnsrewrite.inc

#Config is only read from this directory
#It should mimic the file structure of the real filesystem
CHROOT=/tmp/configroot


makeAndMount()
{
    if [[ ! -d "${1}" ]] ; then
        mkdir -p "${1}"
    fi

    ABS=$(readlink -f ${1})

    mkdir -p "${CHROOT}${ABS}" >/dev/null 2>&1
    umount "${CHROOT}${ABS}" >/dev/null 2>&1
    mount -o bind "${ABS}" "${CHROOT}${ABS}" 
}


makeAndMount ${DNSREWRITEDIR}
makeAndMount ${RUNDIR}
makeAndMount ${ROUTERPLUGINS}

ln -s /tmp ${CHROOT}/var

