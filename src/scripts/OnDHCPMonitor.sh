#!/bin/bash

. /dnsrewrite.inc
exec >>/tmp/onDHCPMonitor
exec 2>&1
echo START $1
date
set -x

#We only work in PRISTINE

if [ "${SYSSTATE}" == "${SYSSTATE_PRISTINE}" ] ; then

#We may be called in 2 circumstances
# 1) DHCP has never succeeded, but we have just been connected
#    in this case, call the lanInfo - which will work out some good IPs for us
#    and then run configureNetwork.sh
#
# 2) Our lease is being renewed for DHCP we have already

    #and only when DHCP has previously failed
    if [[ "${NET_DHCPFAIL}" == "1" ]] ; then
        #This is case 1 - lets go get an address
        if [[ $1 == "bound" || $1 == "renew" ]] ; then
            ${SCRIPTDIR}/lanInfo.sh
            #do not exec this, becuase 
            ${SCRIPTDIR}/configureNetwork.sh
        elif [[ $1 == "deconfig" ]] ; then
            #Nothing to do
            echo "DHCPMonitor triggered with $1 - ignoring"
        else
            echo "DHCPMonitor triggered with $1 - hoping for bound/renew - restarting"
        fi
    else
        #This is case 2 - if the new IPs are the same as our current ones - we can ignore
        echo "DHCPMonitor triggered, for $1 on already DHCP device"
        if [[ $1 == "bound" || $1 == "renew" ]] ; then
            if [[ "${NET_ROUTERIP}" != "$serverid" || "${NET_ASSIGNEDIP}" != "$ip" || "${NET_ASSIGNEDDNS}" != "$dns"  ||  "${NET_GATEWAY}" != "$router" ]] ; then
                ${SCRIPTDIR}/lanInfo.sh
                ${SCRIPTDIR}/configureNetwork.sh
            fi

        fi
    fi
else
        echo "DHCPMonitor triggered, but device is not pristine"
        /etc/init.d/dhcpmonitor stop
fi

