#!/bin/bash

#This script can only be effective when CONFIG_PACKAGE_kmod-ledtrig-gpio is not set


BLUE=7
GREEN=12
MODE=14
RESET=10

#usage gpio.sh GPIONAME [val]

if [[ "$1" = "SETUPLEDS" ]] ; then

    #Blue light
    echo "$BLUE" > /sys/class/gpio/export 2>/dev/null

    #Green light
    echo "$GREEN" > /sys/class/gpio/export 2>/dev/null

    echo "out" > /sys/class/gpio/gpio$BLUE/direction 2>/dev/null
    echo "out" > /sys/class/gpio/gpio$GREEN/direction 2>/dev/null

    exit 0

fi

if [[ "$1" = "SETUPSWITCHES" ]] ; then
    #Mode switch
    echo "$MODE" > /sys/class/gpio/export 2>/dev/null

    #Reset switch
    echo "$RESET" > /sys/class/gpio/export 2>/dev/null


    echo "in" > /sys/class/gpio/gpio$MODE/direction 2>/dev/null
    echo "in" > /sys/class/gpio/gpio$RESET/direction 2>/dev/null

    exit 0
fi

if [[ "$1" = "GREEN" ]] ; then
    echo $2 > /sys/class/gpio/gpio$GREEN/value
    echo "GREEN=$(cat /sys/class/gpio/gpio$GREEN/value)"
elif [[ "$1" = "BLUE" ]] ; then
    echo $2 > /sys/class/gpio/gpio$BLUE/value
    echo "BLUE=$(cat /sys/class/gpio/gpio$BLUE/value)"
elif [[ "$1" = "MODE" ]] ; then
    echo "MODE=$(cat /sys/class/gpio/gpio$MODE/value)"
elif [[ "$1" = "RESET" ]] ; then
    echo "RESET=$(cat /sys/class/gpio/gpio$RESET/value)"
else
    echo "GREEN=$(cat /sys/class/gpio/gpio$GREEN/value)"
    echo "BLUE=$(cat /sys/class/gpio/gpio$BLUE/value)"
    echo "MODE=$(cat /sys/class/gpio/gpio$MODE/value)"
    echo "RESET=$(cat /sys/class/gpio/gpio$RESET/value)"
fi

