#!/bin/bash

. /dnsrewrite.inc

#if running as root - exec as user dnsrewrite or fail
if [[ "${UID}" != "${DNSREWRITE_UID}" ]] ; then
    CMD="${0} $(printf "'%s' " "${@}")"
    exec su -s /bin/bash -c "$CMD" dnsrewrite
fi



#MUST validate parameters (directory traversal)

#SHould consider checking User


#registerDevice.sh MACADDR STATE POLICY


MACADDR="${1}"
STATE="${2}"
NAME="${3}"
POLICY="${3}"



#Validate parameters
DEFPOLICYABSDIR="${DNSREWRITEDIR}/default-policy/"

#[[ "$#" -ge 2 ]] || die "Incorrect no of arguments, $#"

echo ${MACADDR} | grep -E -q '^([0-9A-Fa-f]{2}:){5}([0-9A-Fa-f]{2})$' || die "${MACADDR} does not look like a MAC address"

[[ ${STATE} == ${STATE_UNREGISTERED} ||
   ${STATE} == ${STATE_REGISTERED} ||
   ${STATE} == ${STATE_UNFILTERED} ||
   ${STATE} == ${STATE_FILTERED} ||
   ${STATE} == ${STATE_BLOCKED} ]] || die "Invalid state ${STATE}"

if [[ ${STATE} == ${STATE_FILTERED} ]] ; then
    [[ -n "${POLICY}" ]] || die "Invalid policy"
    echo ${POLICY} | grep -E -q '([.]{2}|[;$/\"])' && die "${POLICY} does not look like a policy 1"
    ls $DEFPOLICYABSDIR | grep -q "${POLICY}" || die "${POLICY} does not look like a policy 2"
fi

if [[ ${STATE} == ${STATE_REGISTERED} ]] ; then
    [[ -n "${NAME}" ]] || die "Invalid name"
    echo ${NAME} | grep -E -q '([.]{2}|[;$/\"])' && die "${NAME} has invalid characters"
fi

DNSTMPDIR="${DHCPDIR}/${MACADDR}"
DEVICEDIR="${DEVICESROOT}/${MACADDR}"
DEFPOLICYLINK="../../default-policy/${POLICY}"




#special handling required for UNREGISTERED state and for changing from UNREGISTERED


if [[ ${STATE} == ${STATE_UNREGISTERED} ]] ; then
    #We are unregistering a device - which may or may not be currently registered
    
    #Remove the DEVICEDIR
    rm -rf ${DEVICEDIR}

    #if an entry exists in the tmp directory - remove its link to the DEVICEDIR, and set its state to unregistered

    if [[ -d ${DNSTMPDIR} ]] ; then
	rm -f ${DNSTMPDIR}/config
	rm -f ${DNSTMPDIR}/state
	echo -n ${STATE_UNREGISTERED} > ${DNSTMPDIR}/state
    fi
elif [[ ${STATE} == ${STATE_UNFILTERED} || ${STATE} == ${STATE_FILTERED} || ${STATE} == ${STATE_BLOCKED} || ${STATE} == ${STATE_REGISTERED} ]] ; then

    if [[ ! -d ${DEVICEDIR} ]] ; then
	#New Device registration
	mkdir -p ${DEVICEDIR}

	#if an entry exists in the tmp directory - device is now registered - hook it up
	if [[ -d ${DNSTMPDIR} ]] ; then
	    rm -f ${DNSTMPDIR}/config
	    ln -s ${DEVICEDIR} ${DNSTMPDIR}/config
	    rm -f ${DNSTMPDIR}/state
	    ln -s ${DEVICEDIR}/state ${DNSTMPDIR}/state
	fi
    else  #Device is previously configured - state probably changing
	:
    fi
    echo -n ${STATE} > ${DEVICEDIR}/state

    if [[ ${STATE} == ${STATE_FILTERED} ]] ; then
	ln -sf ${DEFPOLICYLINK} ${DEVICEDIR}/dnsblock
    else
	#Make sure the old one is gone
	rm -f ${DEVICEDIR}/dnsblock
    fi

    if [[ ${STATE} == ${STATE_REGISTERED} ]] ; then
	#store the device name
	echo -n "${NAME}" > ${DEVICEDIR}/name
    fi
fi

