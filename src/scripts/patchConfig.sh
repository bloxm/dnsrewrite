#!/bin/bash

. /dnsrewrite.inc

CMDLINE=()
CMDLINE+=("sed ${1} ")
for f in ${DNSREWRITEDIR}/laninfo/* ; do
    MATCH="$(basename $f | tr '[a-z]' '[A-Z]')"
    REPLACE="NET_${MATCH}"
    CMDLINE+=( -e '"'s/${MATCH}/${!REPLACE}/g'"')
done

MATCH="PASSWDFILE"
REPLACE="${PASSWDFILE}"
CMDLINE+=( -e '"'s#${MATCH}#${REPLACE}#g'"')

MATCH="SCRIPTDIR"
REPLACE="${SCRIPTDIR}"
CMDLINE+=( -e '"'s#${MATCH}#${REPLACE}#g'"')

MATCH="WIFI_PORTAL"
REPLACE="${WIFI_PORTAL}"
CMDLINE+=( -e '"'s#${MATCH}#${REPLACE}#g'"')

MATCH="APPLIANCESSID"
REPLACE="${APPLIANCESSID}"
CMDLINE+=( -e '"'s#${MATCH}#${REPLACE}#g'"')

MATCH="PASSWORD"
if [[ -f ${PASSWDFILE} ]] ; then
    REPLACE="$(cat ${PASSWDFILE} 2>/dev/null )"
else
    REPLACE="none"
fi
CMDLINE+=( -e '"'s#${MATCH}#${REPLACE}#g'"')

MATCH="USERNAME"
if [[ -f ${ADMINUSERNAMEFILE} ]] ; then
    REPLACE="$(cat ${ADMINUSERNAMEFILE} 2>/dev/null )"
else
    REPLACE="none"
fi
CMDLINE+=( -e '"'s#${MATCH}#${REPLACE}#g'"')


#use underscore - because this is a file path
#CMDLINE+=( -e '"'s_SYSCFGDIR_${SYSCFGDIR}_g'"')

eval ${CMDLINE[@]}
