//convert between filesystem and JSON
/*
 * given a path return a JSON rep of the filesystem, including file contents.
 * given a JSON object and a path, write the JSON filsystem to the disk. (erases everything under the target path)
 *
 *
 
    { "name" : "",
      "links" : [ {"name" : "lname1", "targetpath" : "linktarget", "targetname" : "linktargetname", "broken" : false } ],
      "files" : [ {"name" : "fname1", "data" : [ "line1", "line2", "line3" ] },
                  {"name" : "fname2", "data" : [ "line1", "line2", "line3" ] },
                  {"name" : "fname3", "data" : [ "line1", "line2", "line3" ] } ],
      "children" : [ { "name" : "dir1",
                       "files" : [ {"name" : "fname1", "data" : ["line1"] },
                                   {"name" : "fname2", "data" : ["line1"] },
                                   {"name" : "fname3", "data" : ["line1"] } ],
                       "children" : [] } ]
                   } ]
    }

Uses: https://json-c.github.io/json-c/ 
 
 
 */

#include <json-c/json.h>
#include <stdio.h>
#include <unistd.h>

#include<sys/types.h>
#include<fts.h>
#include<string.h>
#include<errno.h>
#include<ftw.h>
#include <stack>
#include <libgen.h>
#include<stdarg.h>

#include "../utils.h"


//This *must not* start with _BLX_ since it would get removed by the javascript
#define BLX_HASH_NAME "_blx_hash"

//UI is expected to refresh the session token frequently
//Inactive sessions timeout after this period
#define SESSION_MAX_AGE_SECS 75


const char *errLocked="path locked";
const char *errChroot="Unable to chroot";
const char *errPriv="Failed to drop privs";
const char *errFailedToLock="failed to create lock";
const char *errFailedToCreatePath="failed to create path";
const char *errReadingData="failed to read from stdin";
const char *errFailedToParse="Invalid JSON format";
const char *errNone="";

const char *lastError=errNone;

int compare (const FTSENT**, const FTSENT**);
bool dirToJSON(const char *path, bool bWithHTTPHeader, bool bNoFileData, const char *pszSingleFile);
bool JSONtoDir(const char *path, int bytes, bool bWithHTTPHeader, const char *command);
json_object *pathToJSON(const char *path, bool bNoFileData = false, const char *pszSingleFile = NULL);


/* Variables and constants for auth */
const char *auth0="";
const char *authBusy="Already logged in on another device. Please logout from the other session";

bool bSessionHeaderPresent = false;
bool bSessionHeaderEmpty = false;
bool bSessionExpired = false;
bool bSessionExists = false;
bool bSessionMatched = true;
const char *pszBlxAuthMsg = "";

#define LOGLEVEL 9


// Helper macro to convert two-character hex strings to character value
#define DOHEX(Y) (Y>='0'&&Y<='9'?Y-'0':Y-'A'+10)

char* getCGIParam(const char *name, char *value, int bufsize) 
{
    char *pos1 = strstr(getenv("QUERY_STRING"), name);

    char *maxvalue=value+bufsize-1;


    if (pos1) 
    { 
        pos1 += strlen(name);
        if (*pos1 == '=') 
        {
            pos1++;
        
            while (*pos1 && *pos1 != '&' && value < maxvalue)
            { 
                if (*pos1 == '%') 
                {
                    *value++ = (char)DOHEX(pos1[1]) * 16 + DOHEX(pos1[2]);
                    pos1 += 3;
                }
                else if( *pos1=='+' ) 
                {
                    *value++ = ' '; 
                    pos1++; 
                } 
                else 
                {
                    *value++ = *pos1++;
                }
            }
        }
    }
    *value = '\0';

    return value;
} 












//Hash function - djb2
unsigned long
BLX_hash(const char *str)
{
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

void printJSON(const char *str, bool bWithHTTPHeader)
{
    if(bWithHTTPHeader)
    {
        printf("Content-type: application/json\nContent-Length: %d\n", strlen(str));
        printf("\n%s", str);
    }
    else
    {
        printf (str);
    }
}

int unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
    int rv = remove(fpath);

    if (rv)
        perror(fpath);

    return rv;
}

int rmrf(char *path)
{
    return nftw(path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
}

void exitUnauth(const char *msg)
{
    logprintf("exitUnauth\n");
    printf ("Content-Length: 0\nContent-type: text/plain\nStatus: 403 Forbidden\nBLXInfo: %s\n\n", msg);
//    printf ("Content-Length: 0\nContent-type: text/plain\nStatus: 401 Auth failed\n\n");
    exit(0);
}

void exitAuth()
{
    printf ("Content-Length: 3\nContent-type: text/plain\nStatus: 200 Logged In\n\n{}\n");
//    printf ("Content-Length: 0\nContent-type: text/plain\nStatus: 401 Auth failed\n\n");
    exit(0);
}

const char *getConfigPath(const char *config, bool bIsPrivate)
{
    if(bIsPrivate)
    {
        if(strcmp(config, "devices") == 0)
            return getenv("DEVICESROOT");
        else if(strcmp(config, "policies") == 0)
            return getenv("POLICYDIR");
        else if(strcmp(config, "defaultpolicy") == 0)
            return getenv("DEFPOLICYDIR");
        else if(strcmp(config, "dhcp") == 0)
            return getenv("DHCPDIR");
        else if(strcmp(config, "password") == 0)
            return getenv("PASSWDDIR");
        else if(strcmp(config, "session") == 0)
            return getenv("SESSIONDIR");
        else if(strcmp(config, "logs") == 0)
            return getenv("LOGSROOT");
        else if(strcmp(config, "routerplugins") == 0)
            return getenv("ROUTERPLUGINS");
        else if(strcmp(config, "dnsproviders") == 0)
            return getenv("DNSPROVDIR");
        else if(strcmp(config, "catproviders") == 0)
            return getenv("CATPROVDIR");
        else if(strcmp(config, "system") == 0)
            return getenv("CONFIGDIR");
        else if(strcmp(config, "laninfo") == 0)
            return getenv("LANINFO");
    }
    else
    {
        if(strcmp(config, "system") == 0)
            return getenv("CONFIGDIR");
        else if(strcmp(config, "routerplugins") == 0)
            return getenv("ROUTERPLUGINS");
        else if(strcmp(config, "laninfo") == 0)
            return getenv("LANINFO");
        else if(strcmp(config, "password") == 0)
            return getenv("PASSWDDIR");
        else if(strcmp(config, "dnsproviders") == 0)
            return getenv("DNSPROVDIR");
        else if(strcmp(config, "catproviders") == 0)
            return getenv("CATPROVDIR");
    }
    logprintf("No path found for %s, private = %d\n", config, bIsPrivate);

return NULL;
}




//bSessionExists indicates whether someone is logged in
//If this function returns true, either Session key matches or there is no session key
//Use bSessionExists to determine the reason
void verifySessionKey()
{

    const char *sessionFile = getenv("SESSIONFILE");
    const char *blxSession = getenv("HTTP_BLX_SESSION");

    if(!blxSession)
    {
        logprintf("No BLX session key with request\n");
        bSessionHeaderPresent = false;
        bSessionHeaderEmpty = true;
    }
    else
    {
        bSessionHeaderPresent = true;
        bSessionHeaderEmpty = (strlen(blxSession) == 0);
    }

    char sessionKey[64];
    sessionKey[0] = '\0';

    struct stat _stat;
    if(stat(sessionFile, &_stat) == 0)
    {
        time_t currentTime = time(NULL);
        logprintf2("Time check cur = %d file = %d diff = %d\n", currentTime, _stat.st_mtim.tv_sec, currentTime - _stat.st_mtim.tv_sec);
        if((currentTime - _stat.st_mtim.tv_sec) > SESSION_MAX_AGE_SECS)
        {
            //Session has expired
            logprintf("Session has expired\n");
            bSessionExpired = true;
        }
        else
        {
            FILE *fp;
            if((fp = fopen(sessionFile, "r")) != NULL) 
            {
                char *p = fgets(sessionKey, sizeof(sessionKey), fp);
                fclose(fp);
                if(p != sessionKey)
                {
                    sessionKey[0]='\0';
                }
            }

            bSessionExists = strlen(sessionKey) != 0;

            if(bSessionExists && bSessionHeaderPresent && !bSessionHeaderEmpty)
            {
                if(strncmp(sessionKey, blxSession, sizeof(sessionKey)) == 0)
                {
                    bSessionMatched = true;
                }
                else
                {
                    logprintf("Session key didnt match %s - %s - auth fail\n", sessionKey, blxSession);
                    bSessionMatched = false;
                }
            }
            else
            {
                bSessionMatched = false;
            }
        }
    }
}

//If Private is set, this user has already authenticated
//This function is to check the session validity or
//access to a vertain resource, given the state of the appliance
bool authCheck(const char *config, bool bIsPrivate, bool bIsGET, bool &bReadOnly)
{
    logprintf("authCheck(%s, priv=%d, GET=%d)\n", config, bIsPrivate, bIsGET);
    //Access to the password file is restricted, when we are CONFIGURED
    //It can be read, but will be filtered by the JSON transform
    if(!bIsPrivate)
    {
        if(strcmp(config, "password") == 0)
        {
            logprintf("Unauthenticated request for password\n");
            if(!bIsGET && strcmp(getenv("SYSSTATE"), getenv("SYSSTATE_CONFIGURED")) == 0)
            {
                logprintf("Unauthenticated request for password: DENIED\n");
                return false;
            }
            return true;
        }
        else if(strcmp(config, "system") == 0)
        {
            logprintf("SYSTEM %d\n", bIsGET);
            //The system config can be written to unathenticated by the UI only prior to state 'CONFIGURED'
            if(!bIsGET && strcmp(getenv("SYSSTATE"), getenv("SYSSTATE_CONFIGURED")) == 0)
            {
                logprintf("SYSTEM AUTHCHECK RETURNS FALSE\n");
                return false;
            }
            return true;
        }
        else if(strcmp(config, "laninfo") == 0)
        {
            return true;
            //The laninfo can only be read or written while we are in PRISTINE
            //if(strcmp(getenv("SYSSTATE"), getenv("SYSSTATE_PRISTINE")) != 0)
            //{
            //    return true;
            //}
        }
        else if(bIsGET && (strcmp(config, "routerplugins") == 0 || strcmp(config, "dnsproviders") == 0 || strcmp(config, "catproviders") == 0))
        {
            //The routerplugins can only be acquired unauthenticated, while we are in PRISTINE
            if(strcmp(getenv("SYSSTATE"), getenv("SYSSTATE_PRISTINE")) == 0)
            {
                return true;
            }
        }
        logprintf("SYSTEM AUTHCHECK RETURNS DEFAULT FALSE for %s\n", config);
        return false;
    }
    else //Authenticated requests
    {
        logprintf2("Processing authenticated request %s\n", config);
        pszBlxAuthMsg = auth0;

        verifySessionKey();

/*#define DUMP(x) logprintf2("%s = %d\n", #x, x)
        logprintf("Session results\n");
        DUMP(bSessionHeaderPresent);
        DUMP(bSessionHeaderEmpty);
        DUMP(bSessionExpired);
        DUMP(bSessionExists);
        DUMP(bSessionMatched);
        logprintf("END Session results\n");
  */      
        //Basically we only want to allow logins if there is no valid session
        //For login - there MUST be no session at all (i.e no session exists
        //For all other requests, there must be a session
        //Except for logout (which can never fail - or the UI screws up) - all requests must match the current session.
        
        //session is a special case

        
        if(strcmp(config, "session") == 0 && bSessionHeaderPresent && !bSessionHeaderEmpty)
        {
            //Logout should never fail - but we dont want to write the data, if its not authenticated
            logprintf("Config is session\n");
            if(bSessionExpired || !bSessionExists || bSessionMatched)
            {
                logprintf("Allowing\n");
                return true;
            }
            logprintf("Blocking\n");
            pszBlxAuthMsg = authBusy;
            return false;
        }
        else
        {
            //All requests must have a matching session
        logprintf("Session MATCHED\n");
            return bSessionMatched;
        }
    }
    logprintf("Falling through - allowing");
    return true;
}

int main(int argc, char *argv[]) 
{

    openLogFile("/tmp/fsjson.log", "a");
    setLogLevel(LOGLEVEL);

    const char *path = NULL;
    const char *command = NULL;
    const char *sourcepath = NULL;
    int stdin_bytes = 0;
    bool bIsCGI = false;
    bool bIsGET = false;
    bool bIsPrivate = false;
    //When true, the contents of files are not returned
    bool bNoFileData = false;
    char configType[32];

    if(argc == 3)
    {
        path = argv[1];
        command = argv[2];
        bIsGET = true;
    }
    else if(argc == 4)
    {
        path = argv[1];
        command = argv[2];
        stdin_bytes = strtol(argv[3], NULL, 0);
        bIsGET=false;

        logprintf("Buffer size is %d\n", stdin_bytes);
    }
    else if(getenv("REQUEST_METHOD") != NULL)
    {

//        logprintf("Running as CGI\n");
        bIsCGI = true;
        bIsGET = true;

        if(strcmp(getenv("REQUEST_METHOD"), "POST") == 0)
        {
            logprintf("POST\n");
            bIsGET = false;
            if(getenv("CONTENT_LENGTH"))
            {
                stdin_bytes = strtol(getenv("CONTENT_LENGTH"), NULL, 0);
            }
            if(stdin_bytes == 0)
            {
                //We dont accept empty posts
                exitUnauth("No data");
            }
        }

        if(strstr(getenv("SCRIPT_NAME"), "private") != NULL)
        {
            bIsPrivate = true;
            logprintf("Private\n");
        }
        else
        {
            bIsPrivate = false;
        }

        getCGIParam("config", configType, sizeof(configType));

        logprintf("CGI Command %s\n", configType);

        if(!configType)
        {
            exitAuth();
            return true;
        }
        
        if(strcmp(configType, "login") == 0)
        {
            logprintf("Always allow 'login' request\n");
            exitAuth();
            return true;
        }
        else if(strcmp(configType, "logout") == 0)
        {
            logprintf("Always allow 'logout' request\n");
            exitAuth();
            return true;
        }
        else if(strcmp(configType, "routerplugins") == 0)
        {
            if(!bIsGET)
            {
                logprintf("Cant write routerplugins from UI\n");
                exitUnauth("Internal Error 6785");
            }
            bNoFileData = true;
        }
        else if(strcmp(configType, "logs") == 0)
        {
            if(!bIsGET)
            {
                logprintf("Cant write logs from UI\n");
                exitUnauth("Internal Error 6786");
            }
        }
        else if(strcmp(configType, "dnsproviders") == 0)
        {
            if(!bIsGET)
            {
                logprintf("Cant write dnsproviders from UI\n");
                exitUnauth("Internal Error 6787");
            }
        }
        else if(strcmp(configType, "catproviders") == 0)
        {
            if(!bIsGET)
            {
                logprintf("Cant write catproviders from UI\n");
                exitUnauth("Internal Error 6788");
            }
        }

        path = getConfigPath(configType, bIsPrivate);
        command=configType;


        if(!path)
        {
            logprintf("No Path\n");
            exitUnauth("Internal Error 6780");
        }
        logprintf("Checking auth\n");
        logprintf3("Command is %s, Path is %s\n", command, path);
        logprintf("Checking auth2\n");


        bool bReadOnly = false;
        if(!authCheck(configType, bIsPrivate, bIsGET, bReadOnly))
        {
            logprintf("No Auth %s\n", path);
            if(bReadOnly)
            {
                logprintf("Not failing - dropping to read-only mode %s\n", path);
                bIsGET = true;
            }
            else
            {
                exitUnauth(pszBlxAuthMsg);
            }
        }

        if(path)
        {
            logprintf("Will be delivering a path \n");
            logprintf("Will be delivering path %s\n", path);
        }
        else
        {
            logprintf("No path \n");
        }
    }
    else
    {
        return 1;
    }


    bool bProcessSafe = false;

    if(bIsCGI && getuid() == 0)
    {
        //Chroot here - for security
        if(chroot("/tmp/configroot") != 0)
        {
            logprintf("Refusing to process config - cant chroot\n");
            lastError = errChroot;
        }
        else
        {
            chdir("/");

            int groupid=strtol(getenv("DNSREWRITE_GID"), NULL, 0);
            int userid=strtol(getenv("DNSREWRITE_UID"), NULL, 0);

            /* drop privileges */
            if (setgid(groupid) != 0)
            {
                logprintf("Refusing to process config - cant drop group priv\n");
                lastError = errPriv;
            }
            else if (setuid(userid) != 0)
            {
                logprintf("Refusing to process config - cant drop user priv\n");
                lastError = errPriv;
            }
            else if (setuid(0) != -1)
            {
                logprintf("Refusing to process config - managed to regain root priv\n");
                lastError = errPriv;
            }
            else
            {
                bProcessSafe = true;
            }
        }
    }
    else
    {
        bProcessSafe = true;
    }

    bool bSuccess = false;

    if(bProcessSafe)
    {
        logprintf2("Process is safe - processing %s bIsGet = %d path=%s\n", configType, bIsGET, path);
        if (bIsGET) 
        {
            if(strcmp(configType, "logs") == 0)
            {
                char macAddr[20];
                char logIndex[10];
                char devLogPath[128];
                char logFile[16];
                getCGIParam("mac", macAddr, sizeof(macAddr));
                getCGIParam("index", logIndex, sizeof(logIndex));
                if(strlen(logIndex) == 0)
                {
                    logIndex[0] = '1';
                    logIndex[1] = 0;
                }
                
                snprintf(devLogPath, sizeof(devLogPath), "%s/%s/", path, macAddr);
                snprintf(logFile, sizeof(logFile), "%s.log", logIndex);
                logprintf3("handleLog %s, %s\n", devLogPath, logFile);

                bSuccess = dirToJSON(devLogPath, bIsCGI, bNoFileData, logFile);
            }
            else
            {
                logprintf3("dirToJSON %s\n", path);
                bSuccess = dirToJSON(path, bIsCGI, bNoFileData, NULL);
            }
        }
        else if(stdin_bytes > 0)
        {
            logprintf3("JSONtoDir %s(%d)\n", path, stdin_bytes);
            bSuccess = JSONtoDir(path, stdin_bytes, bIsCGI, command);
        }
        else
        {
            logprintf("POST without any data!!!\n");
        }
//        logprintf("DONE\n");
    }
    else
    {
        logprintf("Process not safe\n");
    }

    if(!bSuccess)
    {
        logprintf2("Error handling\n");
        char res[255];
        
        snprintf(res, sizeof(res), "{\"_BLX_success\": false, \"_BLX_reason\": \"%s - %s\"}", basename((char*)path), lastError);


        if(bIsCGI)
        {
            printf ("Content-type: application/json\nContent-Length: %d\n\n%s", strlen(res), res);
        }
        printf(res);
    }

    closeLogFile();
}

void process_file_object(json_object *jFile, const char *path, const char *lockdir, bool bAllowDotOverwrite)
{
    json_object *jName;
    json_object_object_get_ex(jFile, "name", &jName);
    if(jName && json_object_get_string_len(jName))
    {
        FILE *pFile;
        logprintf2("Processing file %s\n", json_object_get_string(jName));
        bool bWriteFile = true;

        if(json_object_get_string(jName)[0] == '.')
        {
            logprintf("Hidden file processing\n");
            //This was a hidden file - we should just copy the original, if it exists
            //or write a new one
            char curDir[256];
            getcwd(curDir, sizeof(curDir));

            if(!bAllowDotOverwrite && strncmp(curDir, lockdir, strlen(lockdir)) == 0)
            {
                char szSourceFile[256];
                snprintf(szSourceFile, sizeof(szSourceFile), "%s/%s%s", path, curDir+strlen(lockdir), json_object_get_string(jName));
                
                int sourceFileSize = 0;

                FILE *in_file = fopen(szSourceFile, "r");
                if(in_file)
                {
                    fseek(in_file, 0L, SEEK_END);
                    sourceFileSize = ftell(in_file);
                    fseek(in_file, 0L, SEEK_SET);
                }

                if(sourceFileSize != 0)
                {
                    bWriteFile = false;


                    char szTargetFile[256];
                    snprintf(szTargetFile, sizeof(szTargetFile), "%s/%s", curDir, json_object_get_string(jName));
                    FILE *out_file = fopen(szTargetFile, "w");
                    char buf[64];

                    logprintf("Copying content from %s to %s\n", szSourceFile, szTargetFile);
                    while (out_file) 
                    {
                        ssize_t result = fread(&buf[0], 1, sizeof(buf), in_file);
                        if (!result) break;
                        if(result > 0)
                        {
                            if(fwrite(&buf[0], 1, result, out_file) != result)
                            {
                                logprintf("Error writing to %s\n", szTargetFile);
                                break;
                            }
                        }
                        else
                        {
                            logprintf("Error reading from %s\n", szSourceFile);
                            break;
                        }
                    }

                    fclose(in_file);
                    fclose(out_file);
                }
            }
        }

        if(bWriteFile)
        {
            logprintf("Writing new file %s\n", json_object_get_string(jName));
            if((pFile = fopen(json_object_get_string(jName), "w")))
            {
                json_object *jData;
                json_object_object_get_ex(jFile, "data", &jData);
                
                //Jdata is an array. Each entry is a line of the file
                int nLines = 0;
                if(jData && (nLines = json_object_array_length(jData)))
                {
                    logprintf("Has %d lines of data\n", nLines);
                    for(int i = 0; i < nLines; ++i)
                    {
                        json_object *pLine = json_object_array_get_idx(jData, i);
                        if(pLine)
                        {
                            fwrite(json_object_get_string(pLine), 1, json_object_get_string_len(pLine), pFile);
                        }
                    }                
                }
                else
                {
                    logprintf("No data to write\n");
                }

                fclose(pFile);
            }
            else
            {
                logprintf("Failed to create file\n");
            }
        }
    }
}

void process_link_object(json_object *jLink)
{
    json_object *jName, *jTargetName, *jTargetPath, *jBroken;
    json_object_object_get_ex(jLink, "name", &jName);
    json_object_object_get_ex(jLink, "targetname", &jTargetName);
    json_object_object_get_ex(jLink, "targetpath", &jTargetPath);
    json_object_object_get_ex(jLink, "broken", &jBroken);

    if(jName && json_object_get_string_len(jName) && 
       jTargetName && json_object_get_string_len(jTargetName) &&
       jTargetPath && json_object_get_string_len(jTargetPath))
    {
        char linkPath[255];
        if(strcmp(json_object_get_string(jTargetPath), ".") == 0 && json_object_get_string_len(jTargetPath) == 1)
        {
            snprintf(linkPath, sizeof(linkPath), "%s", json_object_get_string(jTargetName));
        }
        else
        {
            snprintf(linkPath, sizeof(linkPath), "%s/%s", json_object_get_string(jTargetPath), json_object_get_string(jTargetName));
        }
        logprintf("Creating link %s -> %s\n", json_object_get_string(jName), linkPath);
        symlink(linkPath, json_object_get_string(jName));
    }
}

void parse_dir_object(json_object * jobj, const char *path, const char *lockdir, bool bAllowDotOverwrite) 
{
    json_object *jName, *jFiles, *jLinks, *jChildren;
    json_object_object_get_ex(jobj, "name", &jName);
    json_object_object_get_ex(jobj, "files", &jFiles);
    json_object_object_get_ex(jobj, "links", &jLinks);
    json_object_object_get_ex(jobj, "children", &jChildren);

    //Build my directory on the fly

//  First process any files in the current directory
    int nFiles = 0;
    if(jFiles && (nFiles = json_object_array_length(jFiles)))
    {
        logprintf3("There are %d files\n", nFiles);
        for(int i=0; i < nFiles; ++i)
        {
            json_object *fileObject = json_object_array_get_idx(jFiles, i);
            if(fileObject)
            {
                process_file_object(fileObject, path, lockdir, bAllowDotOverwrite);
            }
        }
    }
//Now process any child directories
    int nChildren = 0;
    if(jChildren && (nChildren = json_object_array_length(jChildren)))
    {
        logprintf3("There are %d children\n", nChildren);
        for(int i=0; i < nChildren; ++i)
        {
            json_object *childObject = json_object_array_get_idx(jChildren, i);
            if(childObject)
            {
                char curDir[256];
                getcwd(curDir, sizeof(curDir));

                if(strlen(curDir) != 0)
                {
                    json_object *jDirName;
                    json_object_object_get_ex(childObject, "name", &jDirName);
                    if(mkdir(json_object_get_string(jDirName), 0755) == 0)
                    {
                        chdir(json_object_get_string(jDirName));
                        parse_dir_object(childObject, path, lockdir, bAllowDotOverwrite);
                    }
                }

                //Restore the current dir
                chdir(curDir);
            }
        }
    }


//Now process any symlinks
    int nLinks = 0;
    if(jLinks && (nLinks = json_object_array_length(jLinks)))
    {
        logprintf3("There are %d links\n", nLinks);
        for(int i=0; i < nLinks; ++i)
        {
            json_object *linkObject = json_object_array_get_idx(jLinks, i);
            if(linkObject)
            {
                process_link_object(linkObject);
            }
        }
    }

} 

bool JSONtoDir(const char *path, int bytes, bool bWithHTTPHeader, const char *command)
{
    char *buf = new char[bytes+1];
    int bytesRemaining = bytes;
    //Read all the data from stdin
    while(bytesRemaining > 0)
    {
        int ret = fread(buf+(bytes-bytesRemaining), 1, bytesRemaining, stdin);
        logprintf("Read %d\n", ret);
        bytesRemaining -= ret;

        if(feof(stdin) || ferror(stdin))
        {
            logprintf("ERROR or EOF\n");
            lastError = errReadingData;
            return false;
        }
    }

    buf[(bytes-bytesRemaining)] = '\0';
    logprintf3("Got: %s\n", buf);

    struct stat statInfo;
    unsigned long origHash = 0;

    //We need to get the hash for our target directory
    //The hash must match from what we are being sent, (so we know config being written is based on the latest version
    //First lock the path - so scripts dont try to update it




    //LOCK PATH
    char lockdir[strlen(path) + 10];
    snprintf(lockdir, sizeof(lockdir), "%s.lock", path);

    if(stat(lockdir, &statInfo) == 0 && S_ISDIR(statInfo.st_mode))
    {
        //Error locked
        //Need to age-out the lock
        bool bLockTooOld = false;
        if(bLockTooOld)
        {
            rmrf(lockdir);
        }
        else
        {
            logprintf2("Refusing to save config - path is locked\n");
            lastError = errLocked;
            return false;
        }
    }

    if(mkdir(lockdir, 0755) != 0)
    {
        logprintf("Failed to create lockdir %s\n", lockdir);
        lastError = errFailedToLock;
        return false;
    }

    if(stat(path, &statInfo) == 0 && S_ISDIR(statInfo.st_mode) || mkdir(path, 0755) == 0)
    {
        json_object * pRoot = json_tokener_parse(buf);
        json_object *currentData = NULL;

        if(!pRoot)
        {
            //Error unable to parse
            logprintf("Unable to parse\n");
            lastError = errFailedToParse;
            rmrf(lockdir);
            return false;
        }

        if(S_ISDIR(statInfo.st_mode))
        {
            //If the path already existed - we need to check that the hash matches what we are being sent
            currentData = pathToJSON(path);

            unsigned long currentHash = BLX_hash(json_object_to_json_string_ext(currentData, JSON_C_TO_STRING_SPACED));
            json_object_object_add(currentData, BLX_HASH_NAME, json_object_new_int(currentHash));

            json_object *jHash;
            json_object_object_get_ex(pRoot, BLX_HASH_NAME, &jHash);

            logprintf3("Object hashes old = %d, new = %d\n", currentHash, json_object_get_int(jHash));
            
            bool bSkipHashCheck = false;
            if(strcmp(command, "session") == 0) //Authentication for session commands is enough!
            {
                bSkipHashCheck = true;
            }

            if(json_object_get_int(jHash) != currentHash)
            {
                logprintf2("Refusing to save config - out-of-date %s\n", path);
                json_object_object_add(currentData, "_BLX_success", json_object_new_boolean(false));
                json_object_object_add(currentData, "_BLX_reason", json_object_new_string("Config out of date"));
                json_object_object_add(currentData, "_BLX_has_config", json_object_new_boolean(true));

                if(bSkipHashCheck)
                {
                    logprintf2("Ignoring out-of-date for command %s %s\n", command, path);
                }
                else
                {
                    //Hash was added above
                    printJSON(json_object_to_json_string_ext(currentData, JSON_C_TO_STRING_SPACED), bWithHTTPHeader);
                    rmrf(lockdir);
                    return true;
                }
            }
        }

        bool bAllowDotOverwrite = false;
        if(strcmp(command, "logout") == 0 || strcmp(command, "password") == 0)
        {
            bAllowDotOverwrite = true;
        }

        char startDir[256];
        getcwd(startDir, sizeof(startDir));

        if(strlen(startDir) && chdir(lockdir) == 0)
        {
            parse_dir_object(pRoot, path, lockdir, bAllowDotOverwrite);
        }

        //Restore the current dir
        chdir(startDir);

        //Config is now written to lockdir
        //Switch the configs over
        char bkpname[strlen(path) + 10];
        snprintf(bkpname, sizeof(bkpname), "%s.old", path);

        char timestamp[strlen(path) + 10];
        snprintf(timestamp, sizeof(timestamp), "%s.tstamp", path);

        //Touch timestamp
        fclose(fopen(timestamp, "w+"));

        //if a previous backupdir exists, remove it
        rmrf(bkpname);

        rename(path, bkpname);
        rename(lockdir, path); //Lockdir is removed

        return dirToJSON(path, bWithHTTPHeader, false, NULL);
    }
    else
    {
        logprintf("Failed to find/create path %s\n", path);
        lastError = errFailedToCreatePath;
    }

    return false;
}



json_object *pathToJSON(const char *path, bool bNoFileData, const char *pszSingleFile)
{
    struct stat statInfo;

    if(stat(path, &statInfo) != 0 || !S_ISDIR(statInfo.st_mode))
        return NULL;

    FTS* file_system = NULL;
    FTSENT *node    = NULL;

    char *const paths[] = {(char *)path, NULL};


    file_system = fts_open(paths, FTS_COMFOLLOW|FTS_NOCHDIR|FTS_NOSTAT, &compare);
    json_object *pRoot = NULL;

    struct obj_ptrs
    {
        json_object *pNode;
        json_object *pFiles;
        json_object *pLinks;
        json_object *pChildren;
        int nLevel;
    };

    std::stack<obj_ptrs> dStack;

    if (NULL != file_system)
    {
        obj_ptrs cur;

        while( (node = fts_read(file_system)) != NULL)
        {
            switch (node->fts_info) 
            {
                case FTS_DP:
                {
                    if(node->fts_level)
                    {
                        cur = dStack.top();
                        dStack.pop();
//                        printf("Popped directory stack for %s, %d\n", node->fts_name, cur.nLevel);
                    }
                }
                break;
                case FTS_D:
                {
//                    printf("Directory %s - level = %d\n", node->fts_name, node->fts_level);
                    
                    json_object *pNewDir = json_object_new_object();

                    if(pRoot == NULL)
                    {
                        pRoot = pNewDir;
                    }
                    else
                    {
                        //Add ourself to the children list of our parent
                        if(cur.pChildren == NULL)
                        {
                            cur.pChildren = json_object_new_array();
                            json_object_object_add(cur.pNode, "children", cur.pChildren);
                        }
                        json_object_array_add(cur.pChildren, pNewDir);
                        dStack.push(cur);
                    }


                    cur.pNode = pNewDir;
                    cur.pLinks = NULL;
                    cur.pFiles = NULL;
                    cur.pChildren = NULL;
                    cur.nLevel = node->fts_level;

                    json_object_object_add(cur.pNode, "name", json_object_new_string(node->fts_name));
                }    
                break;
                case FTS_SL:
                {
                    if(pszSingleFile)
                        break;

                    if(cur.pLinks == NULL)
                    {
                        cur.pLinks = json_object_new_array();
                        json_object_object_add(cur.pNode, "links", cur.pLinks);
                    }
                    struct stat buf;
                    char linkTarget[255];
                    int sz = sizeof(linkTarget);
                    if((sz = readlink(node->fts_accpath, linkTarget, sz)))
                    {
                        linkTarget[sz]='\0';
                    }

                    char linkTarget2[255]; //since basename/dirname may modify args
                    strcpy(linkTarget2, linkTarget);

                    json_object *pNewFile = json_object_new_object();
                    json_object_array_add(cur.pLinks, pNewFile);
                    json_object_object_add(pNewFile, "name", json_object_new_string(node->fts_name));
                    json_object_object_add(pNewFile, "targetpath", json_object_new_string(dirname(linkTarget)));
                    json_object_object_add(pNewFile, "targetname", json_object_new_string(basename(linkTarget2)));
                    json_object_object_add(pNewFile, "broken", json_object_new_boolean(stat (node->fts_accpath, &buf) != 0));
                }
                break;
                case FTS_F:
                {
                    if(pszSingleFile)
                    {
                       if(strcmp(node->fts_name, pszSingleFile) != 0)
                           break;
                    }
                    if(cur.pFiles == NULL)
                    {
                        cur.pFiles = json_object_new_array();
                        json_object_object_add(cur.pNode, "files", cur.pFiles);
                    }
                    //printf("File %s - level = %d\n", node->fts_name, node->fts_level);
                    //
                    //Got a file - add to the file list
                    json_object *pNewFile = json_object_new_object();
                    json_object_array_add(cur.pFiles, pNewFile);
                    json_object_object_add(pNewFile, "name", json_object_new_string(node->fts_name));

                    if(node->fts_name[0] != '.' && !bNoFileData) //Dont show the content of hidden (dot) files
                    {
                        //Now dump the file content, line by line, into an array
                        json_object *pFileContent = json_object_new_array();
                        json_object_object_add(pNewFile, "data", pFileContent);

                        //Open the file, read each line into a string for the array
                        FILE *fp = fopen(node->fts_accpath, "r");
                        if(fp)
                        {
                            //Ensure at least one line is added otherwise there will be nothing for the UI to bind to
                            char buf[256];
                            bool bLineAdded = false;
                            while(fgets(buf, sizeof(buf), fp) != NULL)
                            {
                                //Remove the newline
                                int nNL = strlen(buf) - 1;
                                if(buf[nNL] == '\n')
                                {
                                    buf[nNL] = 0;
                                }
                                bLineAdded = true;
                                json_object_array_add(pFileContent, json_object_new_string(buf));
                            }
                            fclose(fp);
                            
                            if(!bLineAdded){
                                json_object_array_add(pFileContent, json_object_new_string(""));
                            }
                        }
                        else
                        {
                            perror(node->fts_name);
                        }
                    }
                }
                break;
                default:
                {
                    logprintf("Unknown %s - mode= %d, level = %d\n", node ? node->fts_name : "", node ? node->fts_info : 0, node ? node->fts_level : -999);
                }
                break;
            }
        }
        fts_close(file_system);

    }

    return pRoot;

}

bool dirToJSON(const char *path, bool bWithHTTPHeader, bool bNoFileData, const char *pszSingleFile)
{
    json_object * pRoot = pathToJSON(path, bNoFileData, pszSingleFile);

    if(pRoot)
    {
        /*Now printing the json object*/
        const char *retval=json_object_to_json_string_ext(pRoot, JSON_C_TO_STRING_SPACED);
        unsigned long cfg_hash = BLX_hash(retval);
        json_object_object_add(pRoot, "_BLX_success", json_object_new_boolean(true));
        json_object_object_add(pRoot, BLX_HASH_NAME, json_object_new_int(cfg_hash));
        retval=json_object_to_json_string_ext(pRoot, JSON_C_TO_STRING_SPACED);

        printJSON(retval, bWithHTTPHeader);
        return true;
    }
    return 0;
}



int compare(const FTSENT** a, const FTSENT** b)
{
    return (strcmp((*a)->fts_name, (*b)->fts_name));
}

